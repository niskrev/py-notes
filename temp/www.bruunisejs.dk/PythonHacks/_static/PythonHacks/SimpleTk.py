#############################################################################
# The finance package is open source under the `Python Software Foundation  #
# License <http://www.opensource.org/licenses/PythonSoftFoundation.php>`_   #
#############################################################################

'''
********************************
Quick and dirty dialog functions
********************************

:download: `the SimpleTk module <../_static/PythonHacks/SimpleTk.py>`_

The code is open source under the `Python Software Foundation License
<http://www.opensource.org/licenses/PythonSoftFoundation.php>`_

Scripting in VBA/Excel have tought me that you get a long way using simple
dialogs.

For a long while I've never had the need to use dialogs in my Python coding.
When I did, I found that by accident the set of simple dialog functions spread
out into 4 separate modules.

This module unites all simple ask functions from the python modules:

    * tkMessageBox
    * tkSimpleDialog
    * tkColorChooser
    * tkFileDialog

into one single class SimpleDialogTk.

When my usage of dialogs in VBA/Excel went a bit further it had usually to do
with using a set of simple labelled input elements to get a set of values.

I was inspired by `EasyGUI <http://easygui.sourceforge.net>`_. The idea of
hiding the eventdriven principle of tkinter and reduce the GUI to a matter of
calling the GUI functions and getting the input values (the code being linear).

I just want to get the input values for use in the Python code.

My solution consist of the following classes:

    * BaseRoot - Usually it is needed to have the possibility of accepting or
      cancel the input data. That decision is kept in the property input_ok

    * LabelledEntry - A labelled entry field. Return a string

    * LabelledList - A labelled single select listbox. Return a string

    * LabelledCheckButton - A CheckButton build on the same ideas as
      LabelledEntry and LabelledList

Common to all the labelled classes above is that when they are called as
functions they return the value set in the GUI.

To see the code in action there is the following example of an login window:

.. code-block:: none
   :linenos:

    master = BaseRoot('Example of advanced QaDdialog, login')
    user_name = LabelledEntry(master(), 'Enter username:', 'MyUserName')
    password = LabelledEntry(master(), 'Enter password:', get_focus=True, show='*')
    server_names = ['Server%02d' % i for i in range(12)]
    server = LabelledList(master(), 'Choose server:', server_names)
    advanced = LabelledCheckButton(master(), 'Advanced login')
    master().mainloop()
    if master.input_ok:
        print 'Login with: ', user_name(), password(), server(), advanced()
    else:
        print 'Login ignored', user_name(), password(), server(), advanced()

which results in the following GUI (Ubuntu 13.04):

.. image:: 600_On_Python/pictures/SimpleTk_example.png
   :align: center

Commenting the example code above:

    * line  1: Regrettably we still has to use an root element. Here the window
      title is set
    * line  2: The typical arguments for a labelled entry are master, label text
      and a start value (my own login)
    * line  3: If I like the default login there is no need to start at login,
      so focus is set to the password entry. Also the password is not to be
      shown. It will be hidden behind show character
    * line  4: Create a list of server names
    * line  5: Choose server to use from the list of server names
    * line  6: It is also possible simply to check things off using a check
      button
    * line  7: The master returns a root element when called as a function. In
      order to get data one must call the method mainloop of the root
    * line  8: Here it is assumed that the window is closed again. If the window
      is closed with the ok button then the property input_ok of the master is
      true, otherwise it is false. This way one can control wether or not to use
      the entered data
    * line 9-: Here it is assumed that the window is closed again. When the
      input instances are called as functions they return their contents as
      string, except for LabelledCheckButton which returns 0 or 1

'''

import Tkinter as tk
import tkMessageBox
import tkSimpleDialog
import tkColorChooser
import tkFileDialog


class SimpleDialogTk:
    '''SimpleDialogTk is a class containing all simple dialog functions in
    python.
    It unites all simple ask functions from the following python modules:

        * tkMessageBox
        * tkSimpleDialog
        * tkColorChooser
        * tkFileDialog

    The functions are:

        * askcolor - Ask for a color

        * askdirectory - Ask for a directory, and return the file name

        * askfloat - get a float from the user. Arguments are mainly title, the
          dialog title, and prompt, the label text. The return value is a float

        * askinteger - get an integer from the user. Arguments are mainly title,
          the dialog title, and prompt, the label text. The return value is an
          integer

        * askokcancel - Ask if operation should proceed; return true if the
          answer is ok

        * askopenfile - Ask for a filename to open, and returned the opened file.
          File mode can be set, default is 'r'

        * askopenfilename - Ask for a filename to open

        * askopenfilenames - Ask for multiple filenames to open. Returns a list
          of filenames or empty list if cancel button selected

        * askopenfiles - Ask for multiple filenames and return the open file
          objects. Returns a list of open file objects or an empty list if
          cancel selected. File mode can be set, default is 'r'

        * askquestion - Ask a question. Arguments are mainly title and message

        * askretrycancel - Ask if operation should be retried; return true if
          the answer is yes. Arguments are mainly title and message

        * asksaveasfile - Ask for a filename to save as, and returned the opened
          file. File mode can be set, default is 'w'

        * asksaveasfilename - Ask for a filename to save as

        * askstring - get a string from the user. Arguments are mainly title,
          the dialog title, and prompt, the label text. Return value is a string

        * askyesno - Ask a question; return true if the answer is yes. Arguments
          are mainly title and message

        * askyesnocancel - Ask a question; return true if the answer is yes,
          None if cancelled. Arguments are mainly title and message

        * showerror - Show an error message. Arguments are mainly title and
          message

        * showinfo - Show an info message. Arguments are mainly title and
          message

        * showwarning - Show a warning message. Arguments are mainly title and
          message

    To use the functions just do:

    >>> from SimpleTk import SimpleDialogTk
    >>> ask = SimpleDialogTk()
    >>> print ask.askyesno('Demo of SimpleDialogTk', 'Do you like what you see?')

    It results in (Ubuntu 13.04):

    .. image:: 600_On_Python/pictures/askyesno.png
       :align: center

    printing either True or False depending on your answer.
    '''

    def __init__(self):
        root = tk.Tk()
        root.withdraw()
        #root.wm_iconbitmap(r'C:\Python26\DLLs\py.ico')
        method_lst = [(tkSimpleDialog, 'ask'),
                      (tkMessageBox, 'ask'),
                      (tkMessageBox, 'show'),
                      (tkFileDialog, 'ask'),
                      (tkColorChooser, 'ask')
                      ]
        get_methods = lambda module, startswith: [name for name in dir(module)
                                                  if name.startswith(startswith)]
        for module, startswith in method_lst:
            for method_name in get_methods(module, startswith):
                method = getattr(module, method_name)
                setattr(self, method_name, method)


class BaseRoot(tk.Frame):
    '''BaseRoot is basically a base frame with 2 buttons, OK and Quit.
    Pressing the buttons closes the window again.

    Whether the OK or the Quit button is pressed is kept in the property
    input_ok.

    When called as a function it returns a tkinter Tk root element. The mainloop
    method of this element must be called in order to get the input from the
    user.

    :param master_title: Title text of the base window
    :type master_title:  A string value
    '''

    def __init__(self, master_title=''):
        self.root = tk.Tk()
        #self.root.option_add('*Font', '20')
        #self.root.wm_iconbitmap(r'C:\Python26\DLLs\py.ico')
        self.root.title(master_title)
        self.input_ok = False
        tk.Frame.__init__(self, self.root)
        self.pack(side=tk.TOP)
        buttons_frame = tk.Frame(self.root)
        buttons_frame.pack(side=tk.BOTTOM)
        self.add_button(buttons_frame, "OK!", self.ok_and_quit)
        self.add_button(buttons_frame, "Quit!", self.root.destroy)

    def __call__(self):
        return self

    def ok_and_quit(self):
        self.input_ok = True
        self.root.destroy()

    def add_button(self, parent, text, handler):
        bt = tk.Button(parent, text=text, command=handler)
        bt.pack(side=tk.LEFT)
        bt.bind('<Return>', lambda ev: handler())


class _LabelledFrame(tk.Frame):
    '''_LabelledFrame is an internal base frame to help create labelled input
    objects.

    It provides the label and keeps the entered value.

    To work it requires an entry function that returns an instance of one of the
    tkinter variable classes containing what is input for the entry object.
    The function call has to be assigned to the property _value in the
    constructed new input object, ie the __init__ has the code:

        _LabelledFrame.__init__(self, parent, label_text, label_width)
        self._value = input_function(*properties)

    :param parent: The base instance
    :type parent: An instance of the BaseRoot class
    :param label_text: Text to put in the label
    :type label_text: A string value
    :param label_width: Width of the label. Default is 30
    :type label_width: An positive integer
    '''

    def __init__(self, parent, label_text='', label_width=30):
        tk.Frame.__init__(self, parent)
        self.pack(side=tk.TOP, anchor=tk.NW)
        lbl = tk.Label(self, width=label_width, text=label_text, anchor=tk.NE)
        lbl.pack(side=tk.LEFT, anchor=tk.NW)

    def __call__(self):
        if self._value:
            return self._value.get()


def _base_entry(parent, start_value='', get_focus=False, show=''):
    '''A function utilizing the tkinter entry object.

    :param parent: The base instance
    :type parent: An instance of the BaseRoot class
    :param start_value: Text to put in the label
    :type start_value: A string value
    :param get_focus: Indicate whether focus is to be set here. Default is True.
    :type get_focus: A boolean
    :param show: Text to use to hide what is entered
    :type show: A string value of length 1
    :return: one of the tkinter variable classes containing what is entered
    '''
    value = tk.StringVar()
    value.set(start_value)
    entry = tk.Entry(parent, textvariable=value, show=show)
    entry.pack(side=tk.LEFT, anchor=tk.W, fill=tk.X)
    if get_focus:
        entry.focus_force()
    return value


class LabelledEntry(_LabelledFrame):
    '''A labelled entry. A building block for simple GUIs. Requires an instance
    of a BaseRoot class as input.
    After the BaseRoot class instance is closed the entered value can be
    retrieved by calling the instance of the LabelledEntry.

    :param parent: The base instance
    :type parent: An instance of the BaseRoot class
    :param label_text: Text to put in the label
    :type label_text: A string value
    :param start_value: Text to put in the label
    :type start_value: A string value
    :param get_focus: Indicate whether focus is to be set here. Default is True.
    :type get_focus: A boolean
    :param label_width: Width of the label. Default is 20
    :type label_width: An positive integer
    :param show: Text to use to hide what is entered
    :type show: A string value of length 1
    '''

    def __init__(self,
                 parent,
                 label_text='',
                 start_value='',
                 get_focus=False,
                 label_width=20,
                 show=''
                 ):
        _LabelledFrame.__init__(self, parent, label_text, label_width)
        self._value = _base_entry(self, start_value, get_focus, show)


def _base_list(parent, item_lst=[], get_focus=False):
    '''A function utilizing the tkinter Listbox object.

    :param item_lst: The list of choices
    :type item_lst: A list of strings
    :param get_focus: Indicate whether focus is to be set here. Default is True.
    :type get_focus: A boolean
    '''
    parent = parent or tk._default_root
    value = tk.StringVar()
    if item_lst:
        max_rows_in_lstbox = min(len(item_lst), 4)
        lstbox = tk.Listbox(parent,
                            selectmode=tk.SINGLE,
                            relief=tk.SUNKEN,
                            height=max_rows_in_lstbox,
                            exportselection=0
                            )
        if max_rows_in_lstbox < len(item_lst):
            scrollbar = tk.Scrollbar(parent, takefocus=0)
            scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
            scrollbar.config(command=lstbox.yview)
            lstbox.config(yscrollcommand=scrollbar.set)
        for item in item_lst:
            lstbox.insert(tk.END, item)
        lstbox.pack(side=tk.LEFT, anchor=tk.W, fill=tk.X)
        if get_focus:
            lstbox.focus_force()
        on_select = lambda event: value.set(lstbox.get(tk.ACTIVE))
        for event in ['<space>', '<Return>', '<Button-1>']:
            lstbox.bind(event, on_select)
        return value


class LabelledList(_LabelledFrame):
    '''A labelled single select listbox. A building block for simple GUIs.
    Requires an instance of a BaseRoot class as input.
    After the BaseRoot class instance is closed the entered value can be
    retrieved by calling the instance of the LabelledList.

    :param parent: The base instance
    :type parent: An instance of the BaseRoot class
    :param label_text: Text to put in the label
    :type label_text: A string value
    :param item_lst: The list of choices
    :type item_lst: A list of strings
    :param get_focus: Indicate whether focus is to be set here. Default is True.
    :type get_focus: A boolean
    :param label_width: Width of the label. Default is 20
    :type label_width: An positive integer
    '''

    def __init__(self,
                 parent,
                 label_text='',
                 item_lst=[],
                 get_focus=False,
                 label_width=20
                 ):
        _LabelledFrame.__init__(self, parent, label_text, label_width)
        self._value = _base_list(self, item_lst, get_focus)


class LabelledCheckButton(tk.Checkbutton):
    '''A check button that keeps its input. A building block for simple GUIs.
    Requires an instance of a BaseRoot class as input.
    After the BaseRoot class instance is closed the entered value can be
    retrieved by calling the instance of the LabelledCheckButton.
    '''

    def __init__(self, parent, label_text='', start_value=0):
        self._value = tk.IntVar()
        self._value.set(start_value)
        tk.Checkbutton.__init__(self,
                                parent,
                                text=label_text,
                                variable=self._value
                                )
        self.pack(side=tk.LEFT, anchor=tk.W)

    def __call__(self):
        if self._value:
            return self._value.get()


def build_menu_from_dict(dct, root=None):
    '''Build a menu from a dictionary

    **Usage**

    .. code-block:: none
       :linenos:

        import SimpleTk
        from collections import OrderedDict as odct
        root = SimpleTk.tk.Tk()
        menubar = SimpleTk.tk.Menu(root, tearoff=0)
        dialogs = SimpleTk.SimpleDialogTk()

        def quit_and_destroy(): root.destroy(); root.quit()

        submenu_dct = odct([
            ('Say hi 1', lambda: dialogs.askokcancel('Test1', 'Say HI!')),
            ('Say hi 2', lambda: dialogs.askokcancel('Test2', 'Say HI!'))
            ])
        menu_dct = odct([
            ('Say hi menu', submenu_dct),
            ('separator', None),
            ('Close', quit_and_destroy)
            ])
        SimpleTk.build_menu_from_dict(menu_dct, menubar)
        root.config(menu=menubar)
        root.mainloop()
        print 'done'

    '''
    menubase = tk.Menu(root, tearoff=0)
    for key, value in dct.iteritems():
        if key.count('&') == 1 and not key.endswith('&'):
            key_underline = key.index('&')
        else:
            key_underline = None
        key = key.replace('&', '')
        if isinstance(value, dict):
            # tearoff ???
            submenu = tk.Menu(menubase, tearoff=0)
            build_menu_from_dict(value, submenu)
            if key_underline == None:
                menubase.add_cascade(label=key, menu=submenu)
            else:
                menubase.add_cascade(label=key,
                                     underline=key_underline,
                                     menu=submenu
                                     )
        elif value:
            if value:
                if key_underline == None:
                    menubase.add_command(label=key, command=value)
                else:
                    menubase.add_command(label=key,
                                         underline=key_underline,
                                         command=value
                                         )
        else:
            # TO DO doesn't work
            menubase.add_separator()
