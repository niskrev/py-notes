#############################################################################
# The finance package is open source under the `Python Software Foundation  #
# License <http://www.opensource.org/licenses/PythonSoftFoundation.php>`_   #
#############################################################################

'''
*****************************************
Implementing a calculation tree in Python
*****************************************

:download: `the calculationtree module <../_static/PythonHacks/calculationtree.py>`_

The code is open source under the `Python Software Foundation License 
<http://www.opensource.org/licenses/PythonSoftFoundation.php>`_ 

Some thoughts on a calculation trees
====================================

.. topic:: Abstract:

    I got very much inspired when seeing a code note on `how to build
    spreadsheets in Python
    <http://code.activestate.com/recipes/355045-spreadsheet/>`_

    What the author really does is to show how to use the
    `eval function <http://docs.python.org/library/functions.html#eval>`_
    to evaluate a set of formulas, some of them hierachical.

    So this code is about answering the questions:

    * What is good in a spreadsheet model?
    * And what is not so good?
    * And how can this be implemented in Python?

What is good in a spreadsheet model?
------------------------------------

* Interactiveness
* Hierachical formulas makes it easy to build complex structures
* Easy formula generation though relative/absolute addressing and copy/paste
* Visualization and error checking though showing all values in grids
* The possibility of scripting new functions

And what is not so good?
------------------------

* When a set of formulas is build it is often wanted to hide some of the formulas.
  In spreadsheets this can be handled by:

    * show/hide columns and rows
    * Spreadsheet design by dividing sheets into presentation, calculation and 
      input sheets
    * The use of constants, ie named variables not presented in a cell
    
* When a set of formulas is build it is wanted to use the set as a function 
  repeatedly. In spreadsheets this can be handled by:
  
    * Use of scenarios
    * Use of what-if

And how can this be implemented in Python?
------------------------------------------

Off course the answers to the above depends on what you want to do. The idea 
here is to create an abstract data type to use for building and testing eg 
binomial trees and finite difference methods for option pricing in finance.

The prototype is the calculation tree below.
'''

import math
from decimal import Decimal

# As a standard a calculationTree will be able to use all math functions 
mathtools = dict([(name, eval('math.%s' % name)) for name in dir(math)
                  if not name.startswith('__')])

class _formula(str):
    '''A formula is string subclass where the value when defined and printed
    starts with a '=' sign.
    Design inspired by
    `page <http://coding.derkeiler.com/Archive/Python/comp.lang.python/2004-09/2558.html>`_
    

    >>> f = _formula('= a2 + 2')
    >>> f
    'a2 + 2'
    >>> print f
    = a2 + 2
    >>> str(f)
    '= a2 + 2'
    >>> f2 = _formula(f)
    >>> f2
    'a2 + 2'
    >>> print f2
    = a2 + 2
    >>> str(f2)
    '= a2 + 2'
    >>> f3 = _formula('= sin(pi/4)')
    >>> eval(f3, mathtools)
    0.70710678118654746
    '''
    def __new__(self, value):
        if isinstance(value, _formula):
            value = str.__str__(value)
        else:
            value = value.strip(' =')
        return str.__new__(self, value)
    def __str__(self):
        return '= %s' % str.__str__(self)

class _number(Decimal):
    '''A number converts an integer, a float or a Decimal into a Decimal
    
    >>> _number(1)
    Decimal('1')
    >>> _number(1.2)
    Decimal('1.2')
    >>> _number(Decimal('2.345'))
    Decimal('2.345')
    '''
    def __new__(self, value):
        if isinstance(value, (int, long, float)):
            return Decimal('%s' % value)
        elif isinstance(value, (Decimal, _number)):
            return value

class calculationTree:
    '''A calculationTree is build of branches or leafs, who has a key and a 
    value. The value is either a formula (class) for a branch or a number (class) 
    for a leaf.
    A branch is refered to by another branch if the key of the first branch is 
    in the formula of the second branch.
    A root is a branch not refered to by any other branches.
    If a calculationTree is all leafs there vill be no roots.

    The tree can't be circular.
    
    The definition of a calculation tree and adding formulas and values:
    
    >>> ct = calculationTree()
    >>> ct['a1'] = '5'
    >>> ct['a2'] = '= a1*6'
    >>> ct['a3'] = '= a2*7 + a1'
    
    Evaluating formulas:
    
    >>> ct['a3']
    215
    >>> ct['a2']
    30
    >>> ct['b1'] = '= sin(pi/4)'
    >>> ct['b1']
    0.70710678118654746
    
    Showing a formula:
    
    >>> ct.getformula('b1')
    '= sin(pi/4)'
    
    No circular references is allowed:
    
    >>> try:
    ...     ct['a1'] = '= a2'
    ... except Exception, errorTxt:
    ...     print errorTxt
    ...
    Circular reference
    
    There is check for trees not ending in leafs:
    
    >>> ct['a5'] = '= a4'
    >>> ct['a5']
    'key(a4) is not evaluable'
    >>> ct['a6']
    'key(a6) is not evaluable'

    Getting the roots:

    >>> ct.getRoots()
    ['a3', 'b1', 'a5']
    
    Another way seeing the roots and their values:
    
    >>> print ct
    Roots for calculation tree:
    * a3 = 215
    * b1 = 0.707106781187
    * a5 = key(a4) is not evaluable
    
    Find formulas containing a specific key:
    
    >>> print ct.findKeyInFormulas('a4')
    Key(a4) is in the following formulas
    a5 = a4
    
    Showing the evalation tree below a branch:
    
    >>> print ct.showEvaluation('a3')
    |_ a3 = a2*7 + a1 (215)
     |_ a1 = 5 (5)
     |_ a2 = a1*6 (30)
      |_ a1 = 5 (5)
      
    >>> print ct.showEvaluation('b1')
    |_ b1 = sin(pi/4) (0.707106781187)
    '''
    _branchesOrLeafs = {}
    _branches = []
    def __setitem__(self, key, value):
        if not any(key in v for k, v in self._branchesOrLeafs.iteritems() if k in value):
            if isinstance(value, (str, _formula)):
                self._branches.append(key)
                self._branchesOrLeafs[key] = _formula(value)
            else:                
                self._branchesOrLeafs[key] = _number(value)
                if key in self._branches:
                    self._branches.remove(key)
        else:
            raise Exception('Circular reference')
    def __getitem__(self, key):
        if key in self._branchesOrLeafs.keys() or key in mathtools.keys():
            return eval(self._branchesOrLeafs[key], mathtools, self)
        else:
            return 'key(%s) is not evaluable' % key
    def __str__(self):
        roots = self.getRoots()
        header = 'Roots for calculation tree:\n'
        return header + '\n'.join(['* %s = %s' % (root, self.__getitem__(root)) for root in roots])
    def getformula(self, key):
        return str(self._branchesOrLeafs[key])
    def getRoots(self):
        allFormulas = ''.join([self._branchesOrLeafs[key] for key in self._branches])
        return [key for key in self._branches if key not in allFormulas]
    def findKeyInFormulas(self, key):
        branches = [(k, self.getformula(k)) for k in self._branches]
        header = 'Key(%s) is in the following formulas\n' % key
        return header + '\n'.join(['%s %s' % row for row in branches
                                   if key in row[1]])
    def showEvaluation(self, key, level = 0):
        value = self._branchesOrLeafs[key]
        if isinstance(value, _formula):
            out = '%s|_ %s %s (%s)' % (' ' * level, key, value, self.__getitem__(key))
            for key in self._branchesOrLeafs.keys():
                if key in value:
                    out += '\n%s' % self.showEvaluation(key, level + 1)
            return out

if __name__ == '__main__':
    import doctest
    doctest.testmod()
