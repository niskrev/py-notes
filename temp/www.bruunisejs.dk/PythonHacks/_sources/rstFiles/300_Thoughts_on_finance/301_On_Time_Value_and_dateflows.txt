****************************
On Time, Value and dateflows
****************************

.. topic:: Abstract:

	This a presentation of basic concepts underlying financial risk. It is 
	mainly about the concepts of time, daycount method and value.
	The concepts are implemented as python code.

Observations and Basic Concepts
===============================

Observations
------------

First we have a basic observation. It is possible to go into a bank and set up 
a simple loan or deposit.

A simple loan or deposit is getting or placing an amount now and then at some 
future time repay or receive the same amount plus accumulated interest. 
Either way a present value is set equal to a future payment. So we have our 
first basic but very important modelling concept: The present value of a future 
cash flow.

This concept actually implies 2 new concepts: Time and value.


Time
----

In finance the basic time measure is days or dates. On a specific date one part 
delivers something to the other part. This date can be specified in 2 ways:
 
    * As a specific date 
    * As a period from a starting point, typically today

In the Python package finance there is an implementation of both banking day 
calculations and period calculations. 

This module contains a subclass bankdate implementing the typical banking day 
type calculations, i.e.

    * adding a number (integer) of years, month or days to a bankdate
    * finding the difference in years, month or days for 2 bankdates
    * comparing 2 bankdates
    * finding the previous or next valid bankdate taking into account weekends and 
      holidays


Value
-----

`Wikipedia on Value: <http://en.wikipedia.org/wiki/Value_(economics)>`_ 
...value is how much a desired object or condition is worth relative to other 
objects or conditions

`Wikipedia on Money: <http://en.wikipedia.org/wiki/Money#cite_note-0>`_ 
Money is anything that is generally accepted as payment for goods and services 
and repayment of debts. The main functions of money are distinguished as:
 
* A medium of exchange
* A unit of account
* A store of value
* And occasionally, a standard of deferred payment.

So in this context anything with value is defined as money.


The drivers for setting the value of money
------------------------------------------

`Wikipedia on Store of Value: <http://en.wikipedia.org/wiki/Store_of_value>`_
Storage of value is one of several distinct functions of money. ... It is also 
distinct from the medium of exchange function which requires durability when used 
in trade and to minimize fraud opportunities. ... While these items (common 
alternatives for storing value, the author) may be inconvenient to trade daily 
or store, and may vary in value quite significantly, they rarely or never lose 
all value. This is the point of any store of value, to impose a natural risk 
management simply due to inherent stable demand for the underlying asset.

`Wikipedia on Risk Management: <http://en.wikipedia.org/wiki/Risk_management>`_
Risk management is the identification, assessment, and prioritization of risks 
(defined in ISO 31000 as the effect of uncertainty on objectives, whether 
positive or negative) followed by coordinated and economical application of 
resources to minimize, monitor, and control the probability and/or impact of 
unfortunate events or to maximize the realization of opportunities.

Basically there are 2 opposite needs driving the price process:

* The need for concentrate liquidity ie sufficient surplus of value. Eg when 
  buying a house, a firm e.t.c. ie for consumption. This is called borrowing.
* The need for storing liquidity at future times. Ie for postponing consumption. 
  This is called lending

In the following it assumes that 1 sort of money (A currency like EUR, a stock 
like IBM, commodities like wheat etc) is selected.

The law of one price and dateflows
==================================

If 2 similar loan offers are presented from 2 different banks it would be 
expected that the present value for the 2 offers to be allmost similar as well. 
Otherwise the cheapest would be chosen. This is a consequence of the informed 
market. Hence the law of one price.

Even better, if it is possible to borrow to the cheap rate and invest to the 
expensive rate. That way an arbitrage is made. In theory an arbitrage is often 
assumed not to exist. 

.. _dateflow:

.. topic:: Definition, dateflow:

	A dateflow is a set of future T ordered (after time) payments 
	:math:`\left(c_{t_{i}}\right)_{i=1}^{T}` at times 
	:math:`\left(t_{i}\right)_{i=1}^{T}` expressed as dates.
	These payments can be considered vectors. 
	Ie. dateflows that be added and multiplied like normal vectors after 
	filling with zeroes at missing times so that the 2 vectors have the same 
	set of times. So in the following there will be no difference between 
	vectors with ordered keys and dateflows.

The maturity of a dateflow is the biggest time with a non-zero payment.

The concept of dateflows are implemented in the Python module dateflow. 
The times in the dateflow are based of concept banking day described above.

The class dateflow is a set of 1 or more pairs of a bankdate and and a (float) 
value. The operations are

* adding 2 dateflows
* multiplying a dateflow with a number
* removing excessive pairs (identified by a list bankdate of bankdates)

.. topic:: Example:

	Consider a time vector 
	:math:`\overrightarrow{a}=\left(\begin{array}{ccc}1, & 2, & 3\end{array}\right)` 
	at times :math:`\left(\begin{array}{ccc} 2, & 3, & 4\end{array}\right)` 
	and a time vector 
	:math:`\overrightarrow{b}=\left(\begin{array}{ccc}4, & 5, & 6\end{array}\right)` 
	at times :math:`\left(\begin{array}{ccc}1, & 3, & 5\end{array}\right)`.

	In order to add, subtract or multiply :math:`\overrightarrow{a}` and 
	:math:`\overrightarrow{b}` they must first have the same set of times, ie. 
	:math:`\left(\begin{array}{ccccc}1, & 2, & 3, & 4, & 5\end{array}\right)`.
	Then eg.

    .. math::
       :nowrap:

       \begin{eqnarray*}
       \overrightarrow{a}+\overrightarrow{b} 
       & = & \left(\begin{array}{ccccc}0, & 1, & 2, & 3, & 0\end{array}\right)+
             \left(\begin{array}{ccccc}4, & 0, & 5, & 0, & 6\end{array}\right)\\
       & = & \left(\begin{array}{ccccc}4, & 1+0, & 2+5, & 3+0, & 6\end{array}\right)\\
       & = & \left(\begin{array}{ccccc}4, & 1, & 7, & 3, & 6\end{array}\right)
       \end{eqnarray*}

.. _`positive vector`:

.. topic:: Definition, positive vector:

	A vector :math:`\vec{v}=\left(v_{i}\right)_{i=1}^{n}\in\mathbb{R}^{n}` is a 
	row vector and 
	:math:`\vec{v}\geq0\Leftrightarrow v_{i}\geq0\:\forall i\in\left\{ 1,\ldots,n\right\}` 
	ie all vector values are nonnegative
	:math:`\vec{v}>0\Leftrightarrow v_{i}\geq0\:\forall i\in\left\{ 1,\ldots,n\right\}\wedge\exists i:v_{i}>0` 
	ie all vector values are nonzero and at least one 
	vector value is positive 
	:math:`\vec{v}\gg0\Leftrightarrow v_{i}>0\:\forall i\in\left\{ 1,\ldots,n\right\}` 
	ie all vector values are positive

	Off course the definition for negative values is similar, ie:

	:math:`\vec{v}\leq0\Leftrightarrow-\vec{v}\geq0, \vec{v}<0\Leftrightarrow-\vec{v}>0` 
	and :math:`\vec{v}\ll0\Leftrightarrow-\vec{v}\gg0`

It is possible to sell dateflows in order to recieve a (positive) price now and 
vice versa.

.. _`financial market`:

.. topic:: Definition, financial market:

	Consider a selection of N dateflows (dateflow_).
	Every dateflow can be traded at a price :math:`\pi`.

	A financial market is the set of 
	:math:`\left(\vec{\pi}^{\intercal},\bar{C}\right)` where :math:`\vec{\pi}^{\intercal}\in\mathbb{R}^{N}` is a 
	columnvector of nonzero prices.

	And :math:`\bar{C}` is a :math:`N \times T` matrix where each row is a 
	dateflow and all dateflows has been filled with the necessary zeroes so 
	that all dateflows has a value for all dateflow times.

A financial market can be considered as a set of related dateflow instruments, 
eg in the same currency and/or in a specified timespan.

.. _portfolio:

.. topic:: Definition, portfolio:

	A portfolio is a set of amounts of dateflows from a financial market.
	A portfolio :math:`\vec{\theta}, \vec{\theta}\in\mathbb{R}^{N}`, has 
	dateflow :math:`\vec{\theta}\cdot\bar{C}` and price 
	:math:`\vec{\pi}\cdot\vec{\theta}^{\intercal}` or 
	:math:`\vec{\theta}\cdot\vec{\pi}^{\intercal}`.

.. _`Stiemke's Lemma`:

.. topic:: Lemma (Stiemke's Lemma):

	Let :math:`\bar{A}` be a :math:`n \times m` matrix.
	Then exactly one of the following statements are true:

.. math::
   :nowrap:

       \begin{eqnarray}
          \quad\exists\vec{x}\in\mathbb{R}^{m}, \vec{x}\gg0 : 
          \bar{A}\cdot\vec{x}^{\intercal}=0 \\
          \quad\exists\vec{y}\in\mathbb{R}^{n} : 
          \vec{y}\cdot\bar{A}>0
       \end{eqnarray}

*Proof:*
	Assume they both are true. Then
	:math:`\exists\vec{y}\in\mathbb{R}^{n},\exists\vec{x}\in\mathbb{R}^{m},\vec{x}\gg0 : \vec{y}\cdot\left(\bar{A}\cdot\vec{x}^{\intercal}\right)=0`

	and 

	:math:`\vec{y}\cdot\bar{A}>0`

	This can't be true because :math:`\vec{x}\gg0` and :math:`\vec{y}\cdot\bar{A}>0` 

	means that :math:`\vec{y}\cdot\left(\bar{A}\cdot\vec{x}^{\intercal}\right)>0`. 

	Hence the two statements can't be true at the same time.

	Now assume that they are both false, ie

	:math:`\forall\vec{y}\in\mathbb{R}^{n},\forall\vec{x}\in\mathbb{R}^{m},\vec{x}\gg0 : \bar{A}\cdot\vec{x}^{\intercal}\neq0` 
	and :math:`-\vec{y}\cdot\bar{A}>0`

	And again since :math:`\vec{x}\gg0` and :math:`-\vec{y}\cdot\bar{A}>0` 
	means that :math:`\left(-\vec{y}\cdot\bar{A}\right)\cdot\vec{x}^{\intercal}>0\forall\vec{y}`.

	But every vector :math:`\vec{z}\in\mathbb{R}^{n}` in the orthogonal subspace 
	of :math:`\bar{A}\cdot\vec{x}^{\intercal}` will give 
	:math:`\left(-\vec{y}\cdot\bar{A}\right)\cdot\vec{x}^{\intercal}=0`.

	Contradiction again, meaning that at precisely one of the statements must 
	be true.

	Q.E.D.

.. _arbitrage:

.. topic:: Definition, arbitrage:

	An portfolio :math:`\vec{\theta}` is an arbitrage if either the price of 
	the portfolio is zero, ie :math:`\vec{\theta}\cdot\vec{\pi}^{\intercal}=0` 
	and the dateflow is positive at at least one future point, ie 
	:math:`\vec{\theta}\cdot\bar{C}>0` or if the price is negative (giving 
	money to the owner right away) ie. 
	:math:`\vec{\theta}\cdot\vec{\pi}^{\intercal}<0` and maybe also gives the 
	owner a future dateflow, ie. :math:`\vec{\theta}\cdot\bar{C}\geq0`.

	In short a portfolio is an arbitrage iff 
	:math:`\left(-\vec{\theta}\cdot\vec{\pi}^{\intercal},\vec{\theta}\cdot\bar{C}\right)=\vec{\theta}\cdot\left(-\vec{\pi}^{\intercal},\bar{C}\right)>0`.

	A financial market :math:`\left(\vec{\pi}^{\intercal},\bar{C}\right)` is 
	arbitragefree iff there is no arbitrage portfolio in the market.

.. _`Arbitragefree financial market`:

.. topic:: Theorem, arbitragefree financial market:

	A financial market :math:`\left(\vec{\pi}^{\intercal},\bar{C}\right)` is 
	arbitragefree iff there exists a strict positive price vector 
	:math:`\vec{d}\in\mathbb{R}_{+}^{T}` such that 
	:math:`\vec{\pi}^{\intercal}=\bar{C}\cdot\vec{d^{\intercal}}`. 
	Here :math:`\vec{d}` is refered to as the discount vector.

*Proof:*
	From definition of an arbitrage_ we have:

	A financial market :math:`\left(\vec{\pi}^{\intercal},\bar{C}\right)` is 
	arbitragefree iff there is no arbitrage portfolio in the market.

	A portfolio is an arbitrage iff :math:`\left(-\vec{\theta}\cdot\vec{\pi}^{\intercal},\vec{\theta}\cdot\bar{C}\right)=\vec{\theta}\cdot\left(-\vec{\pi}^{\intercal},\bar{C}\right)>0`.

	So there can be no portfolio such that 
	:math:`\vec{\theta}\cdot\left(-\vec{\pi}^{\intercal},\bar{C}\right)>0`. 
	According to `Stiemke's lemma`_ :math:`\exists\vec{x}\in\mathbb{R}^{T},\, x_{0}\in\mathbb{R}, \vec{x}\gg0 :\left(-\vec{\pi}^{\intercal},\bar{C}\right)\cdot\left(x_{0},\vec{x}\right)^{\intercal}=0\Leftrightarrow\bar{C}\cdot\vec{x}^{\intercal}=\vec{\pi}^{\intercal}\cdot x_{0}`.

	Hence :math:`\vec{d}=\frac{1}{x_{0}}\cdot\vec{x}`

	Q.E.D.

.. _`Complete arbitragefree financial market`:

.. topic:: Definition, complete financial market

	A financial market :math:`\left(\vec{\pi}^{\intercal},\bar{C}\right)` is 
	complete iff for all discount vectors :math:`\vec{y}\in\mathbb{R}^{T}` 
	there exists a vector :math:`\vec{x}\in\mathbb{R}^{N}` such that 
	:math:`\vec{x}\cdot\bar{C}=\vec{y}\Leftrightarrow\bar{C}^{\intercal}\cdot\vec{x}^{\intercal}=\vec{y}^{\intercal}`.

From linear algebra it is known that a necessary condition for a market market 
to be complete is that :math:`N \geq T`, ie the number of dateflow in the 
financial market must exceed the number of time points to discount.

A mathematical definition a financial market being complete is that the 
function :math:`f\left(\vec{x}\right)=\bar{C}^{\intercal}\cdot\vec{x}^{\intercal}` 
is surjective.

In words completeness happens when there are more instruments than prices.

.. _`existence of discount factors`:

.. topic:: Theorem, existence of discount factors

	Let a financial market :math:`\left(\vec{\pi}^{\intercal},\bar{C}\right)` 
	be arbitrage free.
	Then it is complete iff there exists a unique vector of discount factors. 

*Proof:*
	Assume first completeness.

	First find the vectors :math:`\vec{z_{t}}\in\mathbb{R}^{N}` that has the 
	unity vector :math:`\vec{e_{t}}`, ie :math:`\vec{z_{t}}\cdot\bar{C}=\vec{e_{t}}`. 
	They exist due to completeness.

	:math:`\bar{Z}` is a :math:`T \times N` matrix having :math:`\vec{z_{t}}` 
	as row vectors and rank T, :math:`T \leq N`.

	Then we have :math:`\bar{Z}\cdot\bar{C}=\bar{I}` and 
	:math:`\vec{\pi}^{\intercal}=\bar{C}\cdot\vec{d^{\intercal}}\Leftrightarrow\bar{Z}\cdot\vec{\pi}^{\intercal}=\vec{d^{\intercal}}` 
	and 
	hence that there can be only 1 vector of discount factors.

	Now assume the uniqueness of the vector of discount factors.

	Further assume that the market is not complete. Then there exists a nonzero 
	vector :math:`\vec{z}\in\mathbb{R}^{T}` such that 
	:math:`\bar{C}\cdot\vec{z}^{\intercal}=\vec{0}`.
	Choose a number a such :math:`\vec{d^{\intercal}}-a\cdot\vec{z^{\intercal}}\gg0`.
	This is a second vector of discount factors which is a contradiction.

	Q.E.D.

.. _`zero bonds`:

.. topic:: Definition, zero bonds:

	The dateflow of a zero bond at time t has value 1 at time t. Hence in a 
	financial market the dateflow of a zero bond is a unit vector 
	:math:`\vec{e_{t}}\in\mathbb{R}^{T}` where the only nonzero value, 1, is 
	at the place reserved for time t.

.. _`discount factor base`:

.. topic:: Theorem, discount factor base for a financial market

	Assume an arbitrage free and complete financial market. Let :math:`\vec{d}` 
	be the unique discount vector.
	Then the price of a zero bond at time t is :math:`d_{t}`, the discount 
	factor reserved for time t.

*Proof:*
	First find the portfolio :math:`\vec{\theta_{t}}` that has the dateflow 
	:math:`\vec{e_{t}}`, ie :math:`\vec{\theta_{t}}\cdot\bar{C}=\vec{e_{t}}`. 
	It exist due to completeness.

	The price for the dateflow is:

	:math:`\vec{\theta}_{t}\cdot\vec{\pi}^{\intercal}=\vec{\theta}_{t}\cdot
	\bar{C}\cdot\vec{d^{\intercal}}=\vec{e_{t}}\cdot\vec{d^{\intercal}}=d_{t}`

	Q.E.D.

A consquence of the theorem is that every instrument is to be considered as a 
portfolio of zero bonds thus making a financial market and portfolios herein to 
portfolios of zero bonds. 

It is possible theoretically to carry on with the concept of a financial market 
:math:`\left(\vec{\pi}^{\intercal},\bar{C}\right)`. The problem is that 
:math:`\bar{C}` soon becomes very big. But ussually the market is not complete.

The number of columns (T) might correspond to every day in mayby 30 or 50 years 
and the problem is to find at least the same amount of trades in order to 
calculate the discount factors.

The essence of the above theorem is that there is a close connection between 
zero bonds and discount factors. That prices are only dependent on duration 
before repayment.

Therefore it would be far better to consider a function which gives a discount 
factor given a time t. The function would typically have a few parameters and 
this makes the estimation based on trades much easier.

.. _`discount factor function`:

.. topic:: Definition, discount (factor) function

    A discount (factor) function returns the price for every future zero bonds
    when there is a zero bond for every future date.


Discount factor functions
=========================

On discount factors and rates on a minimum period
-------------------------------------------------

From the previous chapter the building stones for modeling a discount function
is presented. 

First the notion of a financial market, ie. the model is limited to a set of 
dateflows. These dateflows stems from a set of comparable trades, a notion not 
yet defined.

It is reasonable to assume that there are no arbitrage in a system since no 
one wants to give away money. This implies a strict positive price function.

Also even though the markets typically aren't complete the zero bonds will be 
used as a base for developing a discount function. This function is assumed 
to be unique for a market.

In the model above it is assumed that there is a zero bond for each minimum 
period in the future. This means that the market is complete, so this assumption 
needs to be relaxed at some point.

As a strange thing the modelling of the discount function is based on the rate
concept, ie the loss in value for a future payment quoted in relation to now.
In every other financial quotes one quotes the value of one thing in relation to
the value of something else. Eg USD is qouted in relation EUR or vice versa, and
the IBM stock is quoted in USD etc.
In the end whats matters is what value a future payment has, ie the discount function? And with what certainty or risk the future has? 

Anyway we need to introduce the concept of rate now. Spot rate is the rate 
observed this very minut. Typically some compounded spot rate or a compounded
forward rate. Later we will go further in the definitions of rate.

It is only because prices for future payments are quoted based on rates that we
choose to work with rates and not discount values alone. 

.. _`scalable discount function`:

.. topic:: Definition, scalable discount function:

	A discount function is scalable if :math:`d_t = d(t)=\prod_{i=0}^{t}df_{i}` 
	where :math:`df_{i}` is the future discount factor for the minimum period 
	number i.	
	When observed in the market :math:`d(t)` is the **discrete time compounded 
	spot price for a zero bond**.
	 
	A consequence is that the future discount factor for a period from time
	:math:`t_{1}` to time :math:`t_{2}, t_{1} < t_{2}` can be expressed as 
	:math:`d(t_{1},t_{2})=\prod_{i=t_{1}}^{t_{2}}df_{i}`. Note that 
	:math:`d(t)=d(0,t)`.
	When observed in the market :math:`d(t_{1},t_{2})` is the **discrete time 
	compounded forward price for a zero bond**.
	
	The minimum period might be a day. Sometimes it make more sense to use 
	minimum periods like a month, a quarter or a year.

In the special case where all the :math:`df_{i}`'s are equal the discrete time 
compounded forward price for a zero bond is 

.. math::
    d(t_{1},t_{2})  &= \prod_{i=t_{1}}^{t_{2}}df\\
                    &= df^{(t_{2} - t_{1})}

When borrowing or lending it is expected at least to repay the amount borrowed 
or lended. Futher it is expected that there is an earning and/or a cost coverage 
for having excessive liquidity now. This earning is called the rate and is the 
price for borrowing. 

It is quite easy to see that 

.. math::
     r_{i} &= \frac{1}{df_{i}} - 1 \\ 
           & \Updownarrow \\
    df_{i} &= \frac{1}{\left(1+r_{i}\right)}
            
where :math:`r_{i}` is the price for borrowing one day at day i from now.
Since prices usually are positive, it means that usually :math:`0<df_{i}\leq1`.

In the special case of constant rate discrete time compounded forward price for 
a zero bond is 

.. math::
    d(t_{1},t_{2}) = (1 + r)^{-(t_{2} - t_{1})}` 
    
which the standard textbook formula for discounting using the **Bond rate 
convention**.

The rate can be formulated in different ways:

    * discrete time versus continuous time

    * Continuously compounded discrete rate

    * Forward rate


Instantaneous rate occurs when the minimum period converges to zero for the bond 
rate ie: 
:math:`e^r = \lim_{n \rightarrow \infty}(1+\frac{r}{n})^n`

which can be seen from

.. math::
    r & = r\cdot\lim_{n \rightarrow \infty}\frac{\ln(1+\frac{r}{n}) - 
            \ln(1)}{\frac{r}{n}} \\ 
      & = \lim_{n \rightarrow \infty}\ln((1+\frac{r}{n})^n) \\ 
      & = \ln(\lim_{n \rightarrow \infty}(1+\frac{r}{n})^n)

So one way to combine discount factors and rates are though the exponential
function and instantaneous rates.

Further scalability implies that 

.. math::
    d_{t} & = \prod_{i=0}^{t}df_{i} \\
          & = \exp(\sum_{i=0}^{t}\ln(df_{i})) \\
          & = \exp(-\sum_{i=0}^{t}\ln(1+r_{i}))

which means that a discount factor can be described though the summation or 
integration of the instantaneous rate.

In the special case where the instantaneous rate, ir, is constant, ie 
:math:`ir = -\ln(df_i) = \ln(1 + r_i) \forall i` then

.. math::
    d_{t} & = df^{t} \\
          & = \exp(-ir \cdot t)

which is used for modelling in many finance textbooks.

Instantaneous rates are easy to manipulate when compounding so that is why they 
are used. However rates are usually not quoted as instantaneous rates but rather
as eg bond or swap rates.

In the end it doesn't matter what discounting and daycount (see below) which is 
used. In all cases it will be calibrated to marginally different parameters 
leading to the same set of prices.

.. note::
    The choice here is to use instantaneous rates and continuous discounting 
    since this is the base for most theoretical work.
