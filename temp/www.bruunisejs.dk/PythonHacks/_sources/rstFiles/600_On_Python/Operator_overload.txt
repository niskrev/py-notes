***************************
A Note on Operator Overload
***************************

.. topic:: Abstract:

    Some details on operator overload is described in this note. The text is
    about operator overload in general in python, but it is examplified though
    addition (__add__ and __radd__).

I was testing operator overload when I found this.
Let's start by looking at 2 classes with left operator (addition) overload:

    >>> class A:
    ...     def __add__(self, value):
    ...         return 1
    ... 
    >>> class B:
    ...     def __add__(self, value):
    ...         return 2
    ... 
    >>> a=A()
    >>> b=B()

So let's test the operator overload. A's method __add__ defines that a plus something is 1:

    >>> a+b
    1

And likewise with B. B.__add__ defines that b plus something is 2:

    >>> b+a
    2

Now if I define right addition for A, __radd__:

    >>> A.__radd__ = lambda self, value: 11
    >>> a=A()

To me it would make sense that if I define a new class and a overload operator 
then operator should fully defined by itself. Otherwise I would have to change method __add__ for all the classes I would like to add to from the right.

But if I do:

    >>> a=A()
    >>> a+b
    1
    >>> b+a
    2

then it is still B.__add__ that is used.
Only if I delete B.__add__ method then it done like wanted:

    >>> del B.__add__
    >>> b=B()
    >>> a+b
    1
    >>> b+a
    11

Looking into the documentation all this is also what is supposed to happen.
Method A.__radd__ defines right addition for A when B has no (left) addition 
method B.__add__.

At first I thought that this was an design error. 
It is when I define A I know (and have to decide) which other classes I would 
like to add from both sides.

So I thought of changing the rules to something like: 

.. topic:: Suggestion:

    If A has both __add__ and __radd__ then these methods define the meaning 
    of the operator on A.
    If however that A only has a __add__ method then the method __add__ from B
    should be used.

A problem however might appear the case where both A and B has both methods. 
Which method is then to be used?

Let's see:

    * a+b will use B.__radd__ and A.__add__. But if the suggestion above is 
      followed then B.__radd__ superseeds  A.__add__
    * On the other hand b+a would lead to the use of A.__radd__

So the suggestion above would move the code from __add__ to __radd__, ie nothing 
is gained.

One way to let class A define both sides of addition is by:

    >>> B.__radd__ = lambda self, value: value.__radd__(self) if isinstance(value, A) else 2
    >>> b=B()
    >>> b+a
    11

But this way a hierachy would have to be assumed, eg number are lower than 
vectors, so vectors define both right and left side operator (eg addition) with 
numbers and so on.

Finally note that types like eg int do not use operator overload. If so we would
have had problems with:

    >>> a+1
    1
    >>> 1+a
    11
