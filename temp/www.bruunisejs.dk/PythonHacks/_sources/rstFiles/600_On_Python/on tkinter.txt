**********
On tkinter
**********

.. topic:: Abstract:

    I'm all new to making a graphical user interface in python. Previously I've 
    been working with VBA and excel and there I've surprised on what you can 
    accomodate using functions like messagebox and inputbox.

    And later I found out trying to run Excel from ubuntu and wine that these 
    functions aren't even a part of VBA, but is a part of windows itself.

    I've searched on the internet and found quite a lot saying that tkinter is 
    to primitive. After a first try I must say I do not agree.
    
    This chapter summarizes the set of similar standard functions in python that
    makes it possible to build simple communication to the user. To me this it
    seems that it is a more than sufficient set of functions to handle eg 
    scripting from within eg spreadsheets or other applications.
    
    Here an unnoticed but powerfull set of tools is revealed. 
    
    And it surprises me that this hasn't become the base of smartphone 
    development since it is simple yet highly fexible.


The simple dialog functions in python
=====================================

.. note::

    For all the functions below the Tkinter function Tk has to be running. So 
    the following code (or similar) has to preenter any use of the functions 
    below:
    
        >>> import Tkinter
        >>> root = Tkinter.Tk()
        >>> root.withdraw()
        
    Another way would be simply to install and use the module SimpleTk.

Below a set of simple standard dialog functions to use in user interaction is
described.

In module tkSimpleDialog you get 3 functions:

    * askfloat
    * askinteger
    * askstring
    
They are all called the parameters title, prompt and kw, where the first 2 
parameters are self explained string parameters whereas the last parameter is
actually a set of keywords describing the layout of the dialogs. 
They can be ignored for now.

What is returned should be evident.

In module tkMessageBox you get a set of messageboxes:

    * **askokcancel** - Ask if operation should proceed; return true if the 
      answer is ok
    
    * **askquestion** - Ask a question
    
    * **askretrycancel** - Ask if operation should be retried; return true if 
      the answer is yes
    
    * **askyesno** - Ask a question; return true if the answer is yes
    
    * **askyesnocancel** - Ask a question; return true if the answer is yes, 
      None if cancelled.
    
    * **showerror** - Show an error message
    
    * **showinfo** - Show an info message
    
    * **showwarning** - Show a warning message

Similar to the functions in tkSimpleDialog the functions here at tkMessageBox
are called with parameters title, message and options where the last is to be
ignored for now and the 2 firsts typically are self explaining string values.

If the user has to choose a file name or a path there is the module
tkFileDialog.

In tkFileDialog there are the following methods:

    * **askdirectory** - Ask for a directory, and return the file name
    
    * **askopenfile** - Ask for a filename to open, and returns the opened file 
      in the set mode (first parameter). Default mode is 'r'
    
    * **askopenfilename** - Ask for a filename to open
    
    * **askopenfilenames** - Ask for multiple filenames to open. Returns 
      a list of filenames or empty list if cancel button selected
    
    * **askopenfiles** - Ask for multiple filenames and return the open file 
      objects in the set mode (first parameter). Default mode is 'r'. Returns a 
      list of open file objects or an empty list if cancel selected
    
    * **asksaveasfile** - Ask for a filename to save as, and returns the opened 
      file in the set mode (first parameter). Default mode is 'w'
    
    * **asksaveasfilename** - Ask for a filename to save as
    
All the functions in tkFileDialog also has a second parameter options which
typically can be ignored.

Finally there is the module tkColorChooser with one function askcolor which asks
for a color. A default color can be set as a first parameter.

See also `the SimpleTk module <http://www.bruunisejs.dk/PythonHacks/rstFiles/200%20PythonHacks.html#quick-and-dirty-dialog-functions>`_