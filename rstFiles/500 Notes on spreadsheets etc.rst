#########################
Notes on spreadsheets etc
#########################

Here notes on spreadsheets are kept. Spreadsheets is an interesting framework for 
doing scientific/financial calculations.

.. contents::

.. include:: 500_Notes_on_spreadsheets_etc/
    An example of Method and operator overload in spreadsheets.rst
    
.. include:: 500_Notes_on_spreadsheets_etc/
    enablingGNumericForPython.rst
