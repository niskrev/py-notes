**************
On Yieldcurves
**************


.. topic:: Abstract:

	In the presentation on Time and value the key subject was unique price 
	function. Instead of talking of prices of the cashflows it is customary to 
	talk of the price of lending, i.e. the rate or yield.
	Here we will look into several different types of yield curve definitions.
	These yield curves will be implemented in the finance package.
	
Basic concepts
==============

.. note::
   There no implementation of calibration at this point.

Since most yieldcurve models are specified as a sum of one or more simple yield 
curve functions. 

A yieldcurve function must be a callable class. Also a string representation 
must be defined showing name and used parameters.

The basic design for yieldcurves
================================

Here is the design and the definitions of the functionality of yieldcurve base class in the finance package.

First of all there are some generel functionality: 

* **add_function** It should be possible dynamically to add more yield curve 
  functions to a yield curve

* **yieldcurve_functions** Return the list of used functions

* **Print a yieldcurve** When printed a yield curve shows the string 
  representation for all yield curve functions

And then there are the pure calculation functionality which are defined in the
subsections below.

continous_forward_rate
----------------------

This is the base function from which everything else is derived. At time t it is 
the average rate from now till then.

.. warning::
   Since this is the base a future estimation/calibration procedure must also be 
   based on continous_forward_rate. I.e. this is the function that needs to be 
   optimized.


continous_rate_timeslope
------------------------

The continous_rate_timeslope is the first order derivative with regard to time.
It is used in others calculations. The formula is:

.. math::
   continous\_rate\_timeslope(t) = \frac{\partial continous\_forward\_rate(t)}{\partial t}
	
instantanious_forward_rate
--------------------------

Actually in all textbooks and articles the continous_forward_rate is defined by 
as the average over time of the instantanious_forward_rate. 

But for computational purposes it is better to reverse it. So by definition:

.. math::
   continous\_forward\_rate(t) = \frac{1}{t}\int_{0}^{t}instantanious\_forward\_rate(x)\partial x

And then it follows:

.. math::
   instantanious\_forward\_rate(t) &= \frac{\partial \left(t \cdot continous\_forward\_rate(t)\right)}{\partial t} \\
   &= continous\_forward\_rate(t) \\ 
   & \quad + t \cdot \frac{\partial continous\_forward\_rate(t)}{\partial t} \\
   &= continous\_forward\_rate(t) \\
   & \quad + t \cdot continous\_forward\_timeslope(t)

discount_factor
---------------

The discount_factor is giving the present value of the amount of 1 unit at a 
future time t, P(t). And the formula is:

.. math::
   P(t) &= \exp\left(-\int_{0}^{t}instantanious\_forward\_rate(x)\partial x\right) \\
        &= \exp\left(-continous\_forward\_rate(t) \cdot t\right)
        
When a yieldcurve is called as a python function the yieldcurve will return the 
discount_factor.

zero_coupon_rate
----------------

The definition of zero_coupon_rate comes from the bond and deposit market. It is 
a bit like the continous_forward_rate except for how the discounting is done.
Here the discount_factor is:

.. math::
    P(t) = \left(1 + zero\_coupon\_rate\right)^{-t}

Combining the definition of the discount_factor and the zero_coupon_rate one 
gets:

.. math::
   \exp\left(-continous\_forward\_rate(t) \cdot t\right) = \left(1 + zero\_coupon\_rate\right)^{-t}

or:

.. math::
   continous\_forward\_rate(t) = \ln\left(1 + zero\_coupon\_rate\right)

So there is a simple relation between the continous_forward_rate and the 
zero_coupon_rate. The reverse formula is:
   
.. math::
   zero\_coupon\_rate = \exp (continous\_forward\_rate(t)) - 1

discrete_forward_rate
---------------------

The discrete_forward_rate is the average rate between 2 future times :math:`t_1` 
and :math:`t_2`.
The forward rate is defined as (using no arbitrage):

.. math::
   (1 + zero\_coupon\_rate(t_2))^{t_2} &= (1 + zero\_coupon\_rate(t_1))^{t_1} \\ 
   & \quad \cdot (1 + discrete\_forward\_rate(t_1, t_2))^{t_2 - t_1}

Taking the log gives:

.. math::
   & continous\_forward\_rate(t_2) \cdot t_2 = \\
   & \quad continous\_forward\_rate(t_1) \cdot t_1 \\ 
   & \quad + \ln (1 + discrete\_forward\_rate(t_1, t_2)) \cdot (t_2 - t_1)
   
or

.. math::
   & discrete\_forward\_rate(t_1, t_2) = \\
   & \quad \exp \left(\frac{continous\_forward\_rate(t_2) \cdot t_2
        - continous\_forward\_rate(t_1) \cdot t_1}{t_2 - t_1}\right) - 1

This way the discrete_forward_rate is the descretized time weighted average of the 
continous_forward_rate.

.. note::
   If one of the times are zero then formula reduces to the formula for the 
   zero_coupon_rate

Addding a Parallel Shift or a spread
====================================

Since quotes typically are for the zero_coupon_rate then it makes most sence to 
define a parallel shift, additive_shift to the zero_coupon_rate giving:

.. math::
   continous\_forward\_rate(t, additive\_shift) = 
   \ln\left(1 + zero\_coupon\_rate + additive\_shift\right)

On the other the uses of shift often requires one to find the shift that gives a 
cashflow a certain target value.
In that case it is more feasable to add a shift to the continous_forward_rate. 
**This type will be called a multiplicative shift**. It is worth noting that in 
many textbooks and articles where the authors uses a continous compounding then 
a shift will be multiplicative.

The reason for the name multiplicative_shift is:

.. math::
   P(t, multiplicative\_shift) &= \exp\left(-(continous\_forward\_rate(t) 
                                + multiplicative\_shift) \cdot t\right) \\
                               &= \exp\left(-continous\_forward\_rate(t) \cdot t\right) 
                                \cdot \exp\left(-multiplicative\_shift \cdot t\right) \\
                               &= \left(1 + zero\_coupon\_rate\right)^{-t}
                                \cdot \left(1 + \left(\exp(multiplicative\_shift) - 1)\right)\right)^{-t} \\
                               &= \left(1 + zero\_coupon\_rate\right)^{-t}
                                \cdot \left(1 + multiplicative\_shift\_rate\right)^{-t} \\
                               &= P(t) \cdot \left(1 + multiplicative\_shift\_rate\right)^{-t}

Here :math:`multiplicative\_shift\_rate = \exp(multiplicative\_shift) - 1`.

.. note::
    So now it becomes obvious why the multiplicative shift is mathematically more 
    feasable than the additive. This is because the multiplicative_shift_rate is 
    independent of the original yieldcurve when discounting or compounding. 
    
    In other words the discounting can be done in 2 steps, first discount the 
    cashflows with regard to yieldcurve and discount the discount the discounted 
    cashflows with regard to a simple yieldcurve with a constant rate 
    (the multiplicative_shift_rate).

Anyway the additive_shift cannot be ignored since there are quotes related to it.

The relationship between the additive_shift and the multiplicative_shift_rate 
can be seen from looking at the discount factors for a specific future time t 
expressed though the zero_coupon_rate:

.. math::
    \left(1 + zero\_coupon\_rate + additive\_shift\right)^{-t} &= 
        \left(1 + zero\_coupon\_rate\right)^{-t} 
        \cdot \left(1 + multiplicative\_shift\_rate\right)^{-t} \\
        & \Updownarrow \\
   additive\_shift &= multiplicative\_shift\_rate \cdot (1 + zero\_coupon\_rate)
   
Note however that when additive_shift and multiplicative_shift_rate are calculated 
for more complex cashflows (ie more than 1 payment) then the relation isn't that 
simple.  


