On Cubic and Financial Splines
==============================

The derivation of both the natural and the financial cubic splines are shown at 
other `places <http://www.bruunisejs.dk/PythonHacks/rstFiles/400%20Mathematical%20notes.html#derivation-of-cubic-spline>`_.

Using finance and matplotlib makes it easy to show plots on yieldcurves. 
The data are from [Adams]_:

    >>> times = [0.5, 1, 2, 4, 5, 10, 15, 20]
    >>> rates = [0.0552, 0.06, 0.0682, 0.0801, 0.0843, 0.0931, 0.0912, 0.0857]
    
Then define the functions using finance. First the natural cubic spline:

    >>> import finance
    >>> f1 = finance.yieldcurves.NaturalCubicSpline(times, rates)

And then the financial cubic spline:

    >>> f2 = finance.yieldcurves.FinancialCubicSpline(times, rates)

Then to do the plot below just do:
    
    >>> import matplotlib.pyplot as plt
    >>> plt.plot(times, rates, 'o',
    ...         times, f1.continous_forward_rate(times), '-',
    ...         times, f1.instantanious_forward_rate(times), '--',
    ...         times, f2.continous_forward_rate(times), '-+',
    ...         times, f2.instantanious_forward_rate(times), '--'
    ...         )  # doctest: +SKIP
    >>> plt.legend(['data',
    ...             'natural continous_forward_rate',
    ...             'natural instantanious_forward_rate',
    ...             'financial continous_forward_rate',
    ...             'financial instantanious_forward_rate'
    ...             ], loc='best')  # doctest: +SKIP
    >>> plt.show()

.. image:: 300_Thoughts_on_finance/pictures/splines_zeros_and_forwards.png
   :align: center

In [Adams]_ the key point is that there are some financial inconvieniences at 
the extrapolations beyond the points to the right.

The natural cubic spline is extrapolated as a straight line from the end point 
where the line has the same slope as the curvature at the end point.
In cases like the plot above this leads eventually to negative zero rates as 
well as forward rates.

From a financial viewpoint this not acceptable. Therefore the alternative the 
financial cubic splines is defined.
The idea is here to secure non negative both zero and forward rates. This is 
done by extrapolating from the end point as a horizontal line. This way both 
zero and forward rates are constant from the end point.
The price paid here is strange curvature as seen on the plot above. This leads to inacceptable forward prices.

Another smothness problem arises from the points not being "placed nicely" next 
to each other.
This can eg. be seen for the natural cubic spline by adding a (0, 0) point to (times, rates), ie:

    >>> f1 = finance.yieldcurves.NaturalCubicSpline(times, rates)
    >>> f2 = finance.yieldcurves.NaturalCubicSpline([0] + times, [0] + rates)
    >>> plt.plot(times, rates, 'o',
    ...          times, f1.continous_forward_rate(times), '-',
    ...          times, f2.continous_forward_rate(times), '-'
    ...          )  # doctest: +SKIP
    >>> plt.legend(['data',
    ...             'natural continous_forward_rate',
    ...             'natural continous_forward_rate going through (0, 0) as well'
    ...             ], loc='best')  # doctest: +SKIP
    >>> plt.show()

to get the following plot:

.. image:: 300_Thoughts_on_finance/pictures/trouble_with_splines.png
   :align: center

Again here the "strange" curvature leads to inacceptable prices, eg that prices just before time 1 is higher than they are at time 1.

In order to remedy this some authors suggests a curvature penalty function when estimating. Also some suggests fewer and other time points than the ones offered by the instruments used for estimating.

This way though the splines looses their greatest advantage, ie that the splines goes through the points and near by the points.
