*********************
Day count conventions
*********************

.. topic:: Abstract:

    Here is a very short presentation of the problem of using different time at
    at the same time in finance. More information on this subject will be added 
    in time.

The problem with daily rates is that they are close to zero and especially in 
the old days it meant problems with rounding errors.

Further if someone is lending or borrowing over a longer periods they would 
prefer quotes on years, quarters or months etc.

To be able to compare rates they are typically quoted per year. To get the rate 
per quarter is a matter of dividing the rate quote per year with 4, the monthly 
rate by dividing by 12 etc.

Since days, months and year aren't fully comparable it leads to the notion of 
day count conventions.

In order to do calculations it is necessary to look at time differences and the 
differences has to be a number. That would not cause any troubles if it weren't 
for the fact that price quotes for borrowing are specified pr year, pr half a 
year, pr quarter of a year or pr month etc.

Days, months and years are measures of time differences of incompatible 
definitions, i.e. a month is the time elapsing between successive new moons 
(about 29.53 average days) and a year is the time required for the Earth to 
travel once around the Sun (about 365 1/4 average days).

Therefore it is not possible to move from one time differences measure to 
another, and hence the need for Day Count Conventions, see eg

* `esi on DaycountConventions <http://www.eclipsesoftware.biz/DayCountConventions.html>`_ 
* `Wikipedia on DaycountConventions <http://en.wikipedia.org/wiki/Day_count_convention>`_

QUOTES PER YEAR implies timeconversion to year fractions in order to do calculations

A class `DateToTime <./200%20PythonHacks.html#finance.DateToTime>`_ 
is introduced to implement the most important day count conventions and date 
rolling. Also a valuationday that is chosen.

What it does is given a calculation date and the name of a day count convention 
it returns the time in years and fractions of years
