Where to get Yield Curve Data
=============================

To calibrate yieldcurves yield curve points are needed in the diffent currencies.
Some of these data are free, eg:

  * `U.S. Department of the Treasury <http://www.treasury.gov/resource-center/data-chart-center/interest-rates/Pages/TextView.aspx?data=yield>`_

  * `Euro area yield curve <http://www.ecb.int/stats/money/yc/html/index.en.html>`_

  * `Bank of England yield curve <http://www.bankofengland.co.uk/statistics/yieldcurve/archive.htm>`_
 
It is not always easy to get data like this. For eg. commodities and equities 
one probably has to build yield curves eg from benchmark instruments using
bootstrapping.
