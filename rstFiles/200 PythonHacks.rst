############
Python Hacks
############

This part is reserved for code that might be helpful in the process of doing
financial calculations.

.. automodule:: calculationtree
   :members:

.. automodule:: decimalpy
   :members:
   :inherited-members:

.. automodule:: finance
   :members:

.. automodule:: finance.yieldcurves
   :members:

.. automodule:: mathematical_meta_code
   :members:

.. automodule:: SimpleTk
   :members:



