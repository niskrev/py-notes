###################
Thoughts on finance
###################

Here theoretical notes on finance are placed. This is the theoretical base for
the development of the finance package.

.. contents::

.. include:: 300_Thoughts_on_finance/301_On_Time_Value_and_dateflows.rst

.. include:: 300_Thoughts_on_finance/302_On_Day_count_conventions.rst

.. include:: 300_Thoughts_on_finance/303-00_On_Yieldcurves.rst
	
.. include:: 300_Thoughts_on_finance/303-01_On_NelsonSiegel.rst

.. include:: 300_Thoughts_on_finance/303-02_On_Cubic_and_Financial_Splines.rst

.. include:: 300_Thoughts_on_finance/303-04_Yield_Curve_Data.rst

.. include:: 300_Thoughts_on_finance/303-05_Yield_Curve_derivatives.rst

.. include:: 300_Thoughts_on_finance/303-06_Classical_Interest_Risk.rst

