****************
Splines at Scipy
****************

.. topic:: Abstract:

    This chapter is about splines in Scipy and how they can be used for yield 
    curve calculations.
    
    It is shown that splines in scipy is probably b-splines. These splines 
    gives different (but not nessacerily wrong) values when interpolating.
    
    This information is important when using splines from scipy.

Natural cubic splines 
=====================

The `natural cubic spline <http://en.wikipedia.org/wiki/Natural_cubic_spline>`_ 
is already implemented in  
`decimalpy <../rstFiles/200%20PythonHacks.html#decimalpy.NaturalCubicSpline>`_

In order to redo the calculations below you need to install and import the 
package decimalpy.

>>> import decimalpy as dp
    
The example will be taken from [Kiusalaas]_ p. 119. The data are:

>>> x_data = dp.Vector([1, 2, 3, 4, 5])
>>> y_data = dp.Vector([0, 1, 0, 1, 0])

And the natural cubic spline function is instantiated by:

>>> f = dp.NaturalCubicSpline(x_data, y_data)
    
And the it is possible to do calculations like function values:

    >>> print f(1.5), f(4.5)
    0.7678571428571428571428571427 0.7678571428571428571428571427
    
Slopes:

    >>> print f(1.5, 1), f(4.5, 1)
    1.178571428571428571428571429 -1.178571428571428571428571428

and curvatures:

    >>> print f(1.5, 2), f(4.5, 2)
    -2.142857142857142857142857142 -2.142857142857142857142857143

and it is quite easy to do a plot:

    >>> import matplotlib.pyplot as plt
    >>> linspace = lambda start, stop, steps: dp.Vector([(stop - start) / (steps - 1) * i + start for i in range(steps)])
    >>> xnew = linspace(1, 5, 40)
    >>> x0, spread  = 2, 0.5
    >>> x2new = linspace(x0 - spread, x0 + spread, 40)
    >>> f0x0, f1x0, f2x0 = f(x0, 0), f(x0, 1), f(x0, 2)
    >>> f0Lst = dp.Vector([f(x) for x in xnew])
    >>> f1Lst = dp.Vector([f0x0 + f1x0 * (x - x0) for x in x2new])
    >>> f2Lst = dp.Vector([f0x0 + f1x0 * (x - x0) + f2x0 * (x - x0)**2 / 2 for x in x2new])
    >>> plt.plot(x_data, y_data, 'o', xnew, f0Lst, '-', x2new, f1Lst, '-', x2new, f2Lst, '-')   # doctest: +SKIP
    >>> plt.legend(['data', 'fitted f', 'tangent', '2. order'], loc='best')   # doctest: +SKIP
    >>> plt.show()

like:

.. image:: 400_Mathematical_notes/Graphics/naturalCubicSplineExample.png
   :align: center
   
Splines at Scipy
================

On the other hand [scipy]_ is well equipped with `spline functionality <http://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html>`_

First set up scipy and numpy:

    >>> from numpy import linspace, array, float64
    >>> from scipy.interpolate import InterpolatedUnivariateSpline as spline

Use the same data as before:

    >>> x_data = array([1, 2, 3, 4, 5], float64)
    >>> y_data = array([0, 1, 0, 1, 0], float64)

Creating the cubic spline function: 

    >>> cubic = spline(x_data, y_data, k=3)

And calculate the cubic spline at 1.5 and 4.5:

    >>> cubic(1.5)
    array(1.1250000000000002)
    >>> cubic(4.5)
    array(1.1250000000000002)

This isn't the same value as before (0.767857142857). 

And if one uses interp1d from scipy.interpolate to get the function cubic2:

    >>> from numpy import linspace, array, float64
    >>> from scipy.interpolate import interp1d
    >>> cubic2 = interp1d(x_data, y_data, kind='cubic')

Using cubic2 one gets:

    >>> cubic2(1.5)
    array(1.1944444444444462)
    >>> cubic2(4.5)
    array(1.1944444444444466)

Again a new set of values. What is a bit surprising is that a third set of 
values appears. 

So to check the functions in a graph I do:
 
    >>> xnew = linspace(1, 5, 40)
    >>> import matplotlib.pyplot as plt
    >>> plt.plot(x_data, y_data, 'o', xnew, f0Lst, '-', xnew, cubic(xnew), '--', xnew, cubic2(xnew), '+-')   # doctest: +SKIP
    >>> plt.legend(['data', 'natural spline', 'cubic', 'cubic2'], loc='best')   # doctest: +SKIP
    >>> plt.draw()
    
to get the graph:

.. image:: 400_Mathematical_notes/Graphics/compareNaturalSplineAndScipySpline.png
   :align: center
   
And now it is obvious that scipy isn't using the natural spline as cubic spline.

What kind of spline is used in scipy? Their code is build upon 
`FITPACK <http://www.netlib.org/dierckx/readme>`_ by P. Dierckx.

The main reference is [Dierckx]_ which is all about 
`b-splines <http://en.wikipedia.org/wiki/B-spline>`_. 
So a fair guess is that scipy also is b-splines.

This isn't necessarily bad. Actually I haven't looked into the differences 
between the 2 scipy interpolations.

.. topic:: Conclusion: 

    It matters what kind of interpolation one uses.

    The difference in values is probably due to different derivations of (cubic) 
    splines where scipy interpolation is based on b-splines.
