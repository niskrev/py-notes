**********************************************************************
A note on Gary Foshee Tuesday Problem and related probability problems
**********************************************************************

.. topic:: Abstract:

    There is no more dangerous mathematical discipline than probability theory. 
    And this due to the fact that intuition and actual facts differs as nowhere 
    else.

    This text goes through a list of classical probability paradoxes and it is
    shown **that there actually are no paradoxes, only bad use of conditional 
    probabilities**.

    The problems that will be studied and contrasted are:

        * d'Alembert and the 2 coin tossing
        * The Two Children Problem
        * Getting 2 aces of the same color
        * Monty Hall
        * The thuesday problem
        
    The main error is to reduce the probability space by conditioning some 
    events away. This is first done by d'Alembert and is later done again by
    both Martin Gardner in `"The Two Children Problem" <http://en.wikipedia.org/wiki/Boy_or_Girl_paradox>`_,
    by Gary Foshee in "The Thuesday Child Problem" and by the opponents to vos 
    Savant in the Monty Hall paradox.
    
    What is done by conditioning before answering corresponds to the following.
    Before finding out what color an elephant has, the investigaters paints it 
    red and then conclude that the color of the elephant is.... red.
    
    The mistaken strategy of conditioning before answering is actually quite the 
    oposite to modern use of conditioning in stochastic processes and filtering 
    since here fewer events means that you know less.
    So in that light using the strategy of conditioning before answering you 
    actually decide to get more ignorant before answering.
    And it leads to examples of fake infection of dependency. 

Introduction
============

I read about the tuesday problem in a danish e-magazine, `Ingeniøren <http://ing.dk/artikel/simpel-matematikopgave-gav-laeserstorm-109315>`_.
        
The problem is also described very well in `Science News <http://www.sciencenews.org/view/generic/id/60598/description/When_intuition_and_math_probably_look_wrong>`_.

I was puzzled by the inconsistencies to my perception of probability theory.
Having a master degree in applied probability theory I've come to learn never to
trust your intuition unless you can back it up with mathematical proof and rigor.

And even then it can go wrong. As in the case of Monte Hall I've seen faulty
probability trees used again and again to show the faulty conclusion.

And this brings me to the people that uses computer similations. Because 
simulations build on a wrong probability model verifies nothing just as faulty
probability trees don't prove anything.

So you seldom proves anything by a computer simulation, you just move the 
argument to whether or not your simulation represents the right problem.

The only way is to sit down and think the problems through throroughly.
    
d'Alembert and the 2 coin tossing
=================================

The problem is eg described in `Prakash Gorroochurn:Classic Problems of Probability, chapter 12 <http://books.google.dk/books?id=FBYxYsOo2vkC&pg=PA119&lpg=PA119&dq=probability+2+coins+alembert&source=bl&ots=00eo71N6_A&sig=XlZVgNGezlVFoVQSarVZUwykQa8&hl=da&sa=X&ei=2J0EUr7oDYW44ATDnYHQCg&ved=0CDQQ6AEwATgK#v=onepage&q=probability%202%20coins%20alembert&f=false>`_

.. topic:: d'Alembert and the 2 coin tossing:

    The question slightly reformulated is to calculate the probability of 
    getting 2 tails when tossing 2 coins.

According to d'Alembert there are 3 outcomes {H, TH, TT} in which case the 
probability becomes :math:`\frac{1}{3}` instead of the correct probability 
:math:`\frac{1}{4}`. His argument being that we don't need to toss the second 
coin when the first is Head.

Actually he is answering the question by conditioning. Letting T1 and T2 being
the events of having tail in toss 1 and 2 respectively his answer is 
:math:`P(T1 \cap T2 | T1 \cup T2)=\frac{1}{3}`

And hence d'Alembert is assigning a wrong probability to the outcome H by 
ignoring the outcomes of the second coin.
And hence he is coming to the wrong result by reducing probability space by 
conditioning on it before answering.

Today everyone agrees that d'Alembert is wrong by not using the full and 
original probability space {HH, TH, HT, TT}, but at the time he actually had 
what appeared to be the best arguments.

But this classical error appears again and again in the so called paradoxes 
below. 

The Two Children Problem
========================

We continue with `The Two Children Problem <http://en.wikipedia.org/wiki/Boy_or_Girl_paradox>`_.

This is also analyzed in eg `Grinstead and Snell's Introduction to Probability p. 171 <http://math.dartmouth.edu/~prob/prob/prob.pdf>`_

.. topic:: The Two Children Problem:

    Suppose that Mr. Smith has two children, at least one of whom is a son.
    What is the probability both children are boys?

Let us to start by looking at the probability space of 2 children and whether
or not each child is a boy (B) or a girl (G).

Here we have the events (where i is the order the children are born):

    * B1, B2 is the event that child number 1, 2 is a boy, respectively
    * G1, G2 is the event that child number 1, 2 is a girl, respectively

and  BR is short for :math:`B1 \cap R2`.

The probabilities are:

.. math::

    P(B1)= P(G1) = P(B2) = P(G2) = \frac{1}{2}

The 2 events B1 and B2 are assumed to be independent.

To answer the question we say that in the frase "at least one of whom is a son" 
we don't know whether it is the oldest or the youngest child that is that boy 
mentioned.

Now given that the boy is the oldest then the probability for him to have a
younger brother is due to independence :math:`P(B2|B1) = P(B2) = \frac{1}{2}`.

And hence by similarity:

.. math::

    P(B2|B1) = P(B1|B2) = \frac{1}{2}

Combining this gives :

.. math::

    P(OtherChildIsABoy) &= P(B2|B1) \cdot P(B1) + P(B1|B2) \cdot P(B2) \\
                        &= \frac{1}{2} \cdot \frac{1}{2} + \frac{1}{2} \cdot \frac{1}{2} \\
                        &= \frac{1}{2}

In other words: If we know which child we've got the information about, then the 
sex of the other child remains independent of the first childs sex.
And hence the the probability of the other child being a boy is 
:math:`\frac{1}{2}`. Since we do not which child the information is about we 
have to take the "average" of the probabilities of the other child being a boy 
over child chosen. Still independence is preserved.

So we keep the assumption that the sex of each child is independent of the other.

This result fits into the intuition since knowing the sex of one child
shouldn't influence the probability of the other child.

The other way of looking at it is the one of Gardner's and the referred to in
Ingeniøren. It is similar to the the way d'Alembert wrongfully solved his 
problem.

They are saying given that there is at least 1 boy out of 2 children 
:math:`(B1 \cup B2)` what is the probability of having 2 boys 
:math:`(B1 \cap B2)`.

So they interpret the answer as a conditional probability 
:math:`P(B1 \cap B2 | B1 \cup B2)`.

The probability of at least 1 boy is:

.. math::
    P(B1 \cup B2) = \frac{3}{4} 

And then the answer becomes:

.. math::
    P(B1 \cap B2 | B1 \cup B2) &= \frac{P(B1 \cap B2)}{P(B1 \cup B2)} \\
                               &= \frac{1}{3} 

But conditioning changes the probability space totally as can be seen by:

.. math::

    P(B1 | B1 \cup B2) = \frac{P(B1)}{P(B1 \cup B2)} = \frac{2}{3} \\
    P(B2 | B1 \cup B2) = \frac{P(B2)}{P(B1 \cup B2)} = \frac{2}{3} \\
    P(B1 \cap B2 | B1 \cup B2) = \frac{P(BB)}{P(B1 \cup B2)} = \frac{1}{3} \\

which shows that B1 and B2 no more are independent since:

.. math::

    P(B1 \cap B2 | B1 \cup B2) = \frac{1}{3} \neq \frac{4}{9} = P(B1 | B1 \cup B2) \cdot P(B2 | B1 \cup B2)

This fact becomes the key to handling the thuesday problem later on.
    
So by conditioning by :math:`(B1 \cup B2)` one makes to 2 originally independent
events B1 and B2 dependent.

There is nothing mathematically wrong in the above.

In the end it is all a matter of whether :math:`P(OtherChildIsABoy)` or 
:math:`P(B1 \cap B2 | B1 \cup B2)` is the answer to the question.

But as noted beforeit is easy to see that the answer 
:math:`P(B1 \cap B2 | B1 \cup B2)` is making the same mistake as d'Alembert.

At this point it might be an good idea to dwell at what conditioning actually 
means?

By conditioning you reduce the originally probability space to a new probability
space. 

Since the events in the conditioned probability space is a subset of the
originally space one would expect that other bindings like independence are 
inherited, but they aren't necessarily.

But it is a new probability space where the event GG never happens 
(just like d'Alembert paradox) whenever someone is talking about being parent to
2 children.

And that is why the conditioned probability space shouldn't be used in 
answering the question.

On the other hand it is quite easy to consider the situation where you meet a 
person and one of his/hers children. 

Looking closer you see that the accompanying child is a boy. 
From the conversation following it appears that he/she has another child.

In this case the probability of him/her having 2 boys when it is known that 
he/she has at least one boy is :math:`P(OtherChildIsABoy)=\frac{1}{2}`.

Drawing 2 cards of the same color out of 4 cards
================================================

The next case shows how one sometimes can be mislead by using uniform 
probabilities instead of the right ones.
But conditioning to get the answer can also be a mistake.

.. topic:: Drawing 2 cards of the same color out of 4 cards:

    Draw 2 cards from 4 cards, eg 4 aces. What are the probability of drawing 2 
    of the same color (2 reds or 2 blacks)?

Here we have the events:

    * B1, B2 is the event that card number 1, 2 is black, respectively
    * R1, R2 is the event that card number 1, 2 is red, respectively

and BR is short for :math:`B1 \cap R2`

Here one can be mislead to believe that there are 4 events {BB, BR, RB, RR} with
equal probability leading to the (wrong) answer:

.. math::

    P(BB \cup RR) = \frac{1}{2}
    
But the probabilities are in fact:

.. math::

    P(B1) = P(R1) = \frac{1}{2} \\
    P(B2|B1) = P(R2|R1) = \frac{1}{3} \\
    P(R2|B1) = P(B2|R1) = \frac{2}{3}

leading to:

.. math::

    P(BB) = P(RR) = \frac{1}{6} \\
    P(RB) = P(BR) = \frac{1}{3}

Here the right answer is :math:`P(BB \cup RR) = \frac{1}{3}`
    
The not so known mistake however is to argue for the right probability by using 
P(B2|B1) or P(R2|R1). This is only right due to symmetry.

If there were 5 cards (3 reds and 2 blacks) one would get the probabilities:

.. math::

    P(B1) &= \frac{2}{5} \\
    P(R1) &= \frac{3}{5} \\
    P(B2|B1) &= \frac{1}{4} \\
    P(R2|B1)  &= \frac{3}{4} \\
    P(R2|R1) &= \frac{1}{2} \\
    P(B2|R1) &= \frac{1}{2}

Here the 2 conditional distributions are very different since one conditioning 
on R1 is uniform. So here one can't use P(B2|B1) or P(R2|R1) directly.

Now the above leads to:

.. math::

    P(BB) &= \frac{2}{5} \cdot \frac{1}{4} = \frac{1}{10} \\
    P(BR) &= \frac{2}{5} \cdot \frac{3}{4} = \frac{3}{10} \\
    P(RB) &= \frac{3}{5} \cdot \frac{1}{2} = \frac{3}{10} \\
    P(RR) &= \frac{3}{5} \cdot \frac{1}{2} = \frac{3}{10}

Here the right answer becomes :math:`P(BB \cup RR) = \frac{4}{10}`, which is 
neither :math:`P(B2|B1) = \frac{1}{4}` or :math:`P(R2|R1) = \frac{1}{2}`.

So of course one should use the right underlying probabilities, but one should 
also be carefull about answering in the full probability space.

The Monty Hall problem
======================

Here it is again a question of posing the right questions in order to build the
right probability space. Also here You don't answer the question by conditioning
the probability space before answering. 

.. topic:: `Monty Hall Problem <http://en.wikipedia.org/wiki/Monty_Hall_problem>`_

    Suppose you're on a game show, and you're given the choice of three doors: 
    Behind one door is a car; behind the others, goats. 
    You pick a door, say No. 1, and the host, who knows what's behind the 
    doors, opens another door, say No. 3, which has a goat. He then says to 
    you, "Do you want to pick door No. 2?" Is it to your advantage to switch 
    your choice?
    
The faulty solution says that after a door is opened then there are 2 doors 
with equal probability. And hence there is no gain in choosing the other door.

Here the originally probability space is reduced by the one door opened.

Proper analysis eg in `Grinstead and Snell's Introduction to Probability, p. 136 <http://math.dartmouth.edu/~prob/prob/prob.pdf>`_ 
shows that the proper probability is :math:`\frac{2}{3}` if you choose the other 
door.

The argument being based of the originally probability space with 3 doors is 
shown below.

The columns represents the 3 independent events that describes the whole 
probability space:

* The car is placed randomly behind one of the 3 doors
* A door is chosen by the contestant
* The show host (Monty) opens a door and offers the constestant to choose anew

The probability space is:

======================== ======================== ======================== ==================== =================
Car behind door          Door Chosen              Monty opens              Path Probability     Choose other door
======================== ======================== ======================== ==================== =================
:math:`P(1)=\frac{1}{3}` :math:`P(1)=\frac{1}{3}` :math:`P(2)=\frac{1}{2}` :math:`\frac{1}{18}` Loose
:math:`P(1)=\frac{1}{3}` :math:`P(1)=\frac{1}{3}` :math:`P(3)=\frac{1}{2}` :math:`\frac{1}{18}` Loose
:math:`P(1)=\frac{1}{3}` :math:`P(2)=\frac{1}{3}` :math:`P(3)=1`           :math:`\frac{1}{9}`  Win
:math:`P(1)=\frac{1}{3}` :math:`P(3)=\frac{1}{3}` :math:`P(2)=1`           :math:`\frac{1}{9}`  Win
:math:`P(2)=\frac{1}{3}` :math:`P(1)=\frac{1}{3}` :math:`P(3)=1`           :math:`\frac{1}{9}`  Win
:math:`P(2)=\frac{1}{3}` :math:`P(2)=\frac{1}{3}` :math:`P(2)=\frac{1}{2}` :math:`\frac{1}{18}` Loose
:math:`P(2)=\frac{1}{3}` :math:`P(2)=\frac{1}{3}` :math:`P(3)=\frac{1}{2}` :math:`\frac{1}{18}` Loose
:math:`P(2)=\frac{1}{3}` :math:`P(3)=\frac{1}{3}` :math:`P(1)=1`           :math:`\frac{1}{9}`  Win
:math:`P(3)=\frac{1}{3}` :math:`P(3)=\frac{1}{3}` :math:`P(2)=\frac{1}{2}` :math:`\frac{1}{18}` Loose
:math:`P(3)=\frac{1}{3}` :math:`P(3)=\frac{1}{3}` :math:`P(3)=\frac{1}{2}` :math:`\frac{1}{18}` Loose
:math:`P(3)=\frac{1}{3}` :math:`P(1)=\frac{1}{3}` :math:`P(2)=1`           :math:`\frac{1}{9}`  Win
:math:`P(3)=\frac{1}{3}` :math:`P(2)=\frac{1}{3}` :math:`P(1)=1`           :math:`\frac{1}{9}`  Win
======================== ======================== ======================== ==================== =================

And hence we have :math:`P(WinByChoosingOtherDoor)=\frac{4}{9}=\frac{2}{3}` and 
then it is better to choose the other door.

The Thuesday Child Problem
==========================

Now we are ready for the Thuesday Child Problem. It is an expansion of the 2
children paradox above.

.. topic:: The Thuesday Child Problem:

    Suppose that a person has two children, at least one of whom is a son born 
    on a Thuesday.
    What is the probability both children are boys?

One should imagine that the information on which day the child was born was 
irrelevant. But when conditioning on the probability space before answering one 
by accident introduces a false dependency between birth day of week and the sex
of the child and hence some change in the probabilities.

As seen at 2 Children Paradox this is the wrong way to answer the question.
After showing the wrong arguments leading to the paradox, the right way to do 
the calculations are shown.

Finally I show how adding more and more irrelevant information actually leads
to a convergence to the right probability.

First some notation. There are 2 children, 1 is short for the oldest and 2 is 
short of the youngest. Each child can be a boy (B) or a girl (G). Finally each
child can be born on a Thuesday (T) or at any other day of the week (O).

It is assumed that child order, child sex and child birth day are independent.

In short:

    * 1G is first child is a girl
    * 1T is first child is born on a Thuesday
    * 1BT is first child is a boy born on a Thuesday
    * 1BT2GO is first child is a boy born on a Thuesday and second child is a 
      girl born on any other day than Thuesday
    * etc.

And the basic probabilities are:

.. math::
    P(B) &= P(G) = \frac{1}{2} \\
    P(T) &= \frac{1}{7} \\
    P(O) &= \frac{6}{7} \\

As in the 2 Children Problem we choose to ignore all events not having at least
one boy born on a Thuesday as an event, ie we condition with :math:`1BT \cup 2BT`.

Below is a probability summary:

.. image:: 400_Mathematical_notes/Graphics/GaryFosheeThuesdayChild.png
   :align: center
   
Now:

.. math::

    P(1BT) &= P(2BT) = P(B) \cdot P(T) = \frac{1}{14} \\
    P(1BT \cap 2BT) &= P(1BT) \cdot P(2BT) = \frac{1}{196} \\
    P(1BT \cup 2BT) &= P(1BT) + P(2BT) - P(1BT \cap 2BT) = \frac{27}{196}

This is anything but the grey area in the probability summary.

And the probability of 2 boys, ie the other child is a boy, at least one of them 
born on a Thuesday is:

.. math::
    P(\{1BT2BT, 1BO2BT, 1BT2BO\}) = \frac{1 + 6 + 6}{196} = \frac{13}{196}

This is the green area in the probability summary.

And hence (ignoring the grey area):

.. math::
    P(\{1BT2BT, 1BO2BT, 1BT2BO\} | 1BT \cup 2BT) = \frac{\frac{13}{196}}{\frac{27}{196}} = \frac{13}{27}

So the probability of the other child :math:`\frac{13}{27}`, a little less than
the right value :math:`\frac{1}{2}` and actually quite far from the 
:math:`\frac{1}{3}` that would be expected from the 2 Children Problem.

Let us add a paradox to the paradox. Now what happens if we didn't knew P(T), but 
set it to p. Then P(O) = 1 - p.

.. math::
    P(1BT) &= P(2BT) = P(B) \cdot P(T) = \frac{p}{2} \\
    P(1BT \cap 2BT) &= P(1BT) \cdot P(2BT) = p^2 \\
    P(1BT \cup 2BT) &= P(1BT) + P(2BT) - P(1BT \cap 2BT) = p - \frac{p^2}{4} = \frac{-p^2 + 4 \cdot p}{4} \\ 
    P(\{1BT2BT, 1BO2BT, 1BT2BO\}) &= \frac{p^2 + (1-p) \cdot p + p \cdot (1-p)}{4} = \frac{-p^2 + 2 \cdot p}{4}

This leads to:

.. math::
    P(\{1BT2BT, 1BO2BT, 1BT2BO\} | 1BT \cup 2BT) = \frac{-p^2 + 2 \cdot p}{-p^2 + 4 \cdot p}
    
Now if we make the event more rare, eg saying that the child was born on aprils 
fool, then p would become smaller, eg :math:`\frac{1}{365}`. Then we see that:

.. math::
    P(\{1BT2BT, 1BO2BT, 1BT2BO\} | 1BT \cup 2BT)_{p \rightarrow 0} \rightarrow \frac{1}{2}

Which can be seen easily by the use of the rule of l'Hospital.

Now making the event more rare corresponds to adding more and more irrelevant 
data, ie noise.

And what is seen from the above limit is that the more noise that is added the 
closer we get to the right probability :math:`\frac{1}{2}` (not the wrong 
probability from the 2 Children Problem, :math:`\frac{1}{3}`).

Actually the function :math:`f(p) = \frac{-p^2 + 2 \cdot p}{-p^2 + 4 \cdot p}`
is monotone decreasing in the interval [0, 1] and the minimum 
:math:`f(1) = \frac{1}{3}`.

So the probability of the other child being a boy too is dependent on the amount
of noice added. And in the one extreme case of no noice we get the Gardner case,
:math:`\frac{1}{3}` and in the other extreme of total noice we get the 
probability from independency of sexes, :math:`\frac{1}{2}`.

**This makes no sence! The probability should be constant, ie independent of 
noice. And it is constant if calculations are done properly!**

Let us see how one should do the calculations. The argument is similar 
to the 2 Children problem.

If we known which child who is the thuesday boy then the answer is simple since 
the sex of the other child is independent of whatever can be said about the 
thuesday child.

Define 1BT, 2BT as the event that child 1, 2 is the thuesday boy, respectively.
And since the children are chosen randomly we have 
:math:`P(1BT)=P(2BT) = \frac{1}{2}`.

And then we have:

.. math::
    P(2B | 1BT)  &= P(2B) = \frac{1}{2} \\
    P(1B | 2BT)  &= P(1B) = \frac{1}{2} \\
    P(OtherChildIsABoy) &= P(2B | 1BT) \cdot P(1BT) + P(1B | 2BT) \cdot P(2BT) = \frac{1}{2}

**I rest my case :)**
