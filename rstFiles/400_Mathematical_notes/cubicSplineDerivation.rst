**************************
Derivation of Cubic Spline
**************************

.. topic:: Abstract:

    This chapter is about derivation of cubic splines and the use of cubic 
    splines for yield curve calculation.

Deriving the cubic spline function
==================================

Assume n points :math:`((x_{i},y_{i}))_{i=1}^{n}` and n curvatures 
:math:`(k_{i})_{i=1}^{n}` at each point.

To ease notation we introduce:

.. math::
    \triangle x_{i}=x_{i+1}-x_{i}

The point are known, the curvatures are assumed known

.. math::
    f_{i}''(x) & = \frac{x-x_{i}}{\triangle x_{i}}\cdot k_{i+1}+\frac{x_{i+1}-x}{\triangle x_{i}}\cdot k_{i}\\
               & = \frac{(x-x_{i})\cdot k_{i+1}+(x_{i+1}-x)\cdot k_{i}}{\triangle x_{i}}

Because we want to evaluate f,f',f'' at :math:`x_{i}` it makes things easier 
when they are formulated as functions of :math:`(x-x_{i})` and 
:math:`(x-x_{i+1})`. Eg it is quite simple to see that:

.. math::
    f_{i-1}''(x_{i})=f_{i}''(x_{i})=k_{i}

So the second derivative is continous and piecewise linear.

Also the first derivative f' can be formulated as (since 
:math:`\int(a\cdot x-b)\partial x=\frac{1}{2a}\cdot(a\cdot x-b)^{2}+c)`:

.. math::
    f_{i}'(x)=\frac{(x-x_{i})^{2}\cdot k_{i+1}-(x_{i+1}-x)^{2}\cdot k_{i}}{2\cdot\triangle x_{i}}+c_{i}

The function :math:`f_{i}` which lies :math:`x_{i}` and :math:`x_{i+1}` for 
:math:`i=1,\ldots,n-1` must look like (integrating twice):

.. math::
    f_{i}(x) & = \frac{(x-x_{i})^{3}\cdot k_{i+1}+(x_{i+1}-x)^{3}\cdot k_{i}}{6\cdot\triangle x_{i}}+c_{i}\cdot x+d_{i}\\
             & = \frac{(x-x_{i})^{3}\cdot k_{i+1}+(x_{i+1}-x)^{3}\cdot k_{i}}{6\cdot\triangle x_{i}}+a_{i}\cdot(x_{i+1}-x)+b_{i}\cdot(x-x_{i})

where :math:`c_{i}=-a_{i}+b_{i}` and 
:math:`d_{i}=a_{i}\cdot x_{i+1}-b_{i}\cdot x_{i}` or

.. math::
    \left(\begin{array}{c}\begin{array}{c}c_{i}\end{array}\\d_{i}\end{array}\right)=\left(\begin{array}{cc}-1 & 1\\x_{i+1} & -x_{i}\end{array}\right)\cdot\left(\begin{array}{c}a_{i}\\b_{i}\end{array}\right)

This is possible if the determinant :math:`x_{i}-x_{i+1}\neq0`. This is the case 
if there isn't 2 points with the same x-value. An asumption which already must 
be in place.

And since f must be continous and go through the points 
:math:`((x_{i},y_{i}))_{i=1}^{n}` we have:

.. math::
    f_{i}(x_{i}) & = y_{i}\\
    \Updownarrow\\
    \frac{(x_{i}-x_{i})^{3}\cdot k_{i+1}+\triangle x_{i}^{3}\cdot k_{i}}{6\cdot\triangle x_{i}}+a_{i}\cdot\triangle x_{i}+b_{i}\cdot(x_{i}-x_{i}) & = y_{i}\\
    \Updownarrow\\
    \frac{\triangle x_{i}\cdot k_{i}}{6}+a_{i} & = \frac{y_{i}}{\triangle x_{i}}\\    
    \Updownarrow\\
    \frac{y_{i}}{\triangle x_{i}}-\frac{\triangle x_{i}\cdot k_{i}}{6} & = a_{i}

and

.. math::
    f_{i}(x_{i+1}) & = y_{i+1}\\
    \Updownarrow\\
    \frac{\triangle x_{i}^{3}\cdot k_{i+1}+(x_{i+1}-x_{i+1})^{3}\cdot k_{i}}{6\cdot\triangle x_{i}}+a_{i}\cdot(x_{i+1}-x_{i+1})+b_{i}\cdot\triangle x_{i} & = y_{i+1}\\    
    \Updownarrow\\
    \frac{y_{i+1}}{\triangle x_{i}}-\frac{\triangle x_{i}\cdot k_{i+1}}{6} & = b_{i}

This means that the constant :math:`c_{i}`:

.. math::
    c_{i} & = -a_{i}+b_{i}\\
          & = -(\frac{y_{i}}{\triangle x_{i}}-\frac{\triangle x_{i}\cdot k_{i}}{6})+\frac{y_{i+1}}{\triangle x_{i}}-\frac{\triangle x_{i}\cdot k_{i+1}}{6}\\ 
          & = \frac{\triangle y_{i}}{\triangle x_{i}}-\frac{\triangle x_{i}\cdot\triangle k_{i}}{6}

Ekstrapolation
==============

The question is what to do when there is a need for extrapolation. This is a 
situation quite common when talking about yieldcurve. Eg there are observed
zero coupons up until year 20 but a zero coupon for year 25 is needed.

Talking about the natural cubic spline and financial cubic spline they are just 
linear extrapolations of the endpoints with the same slope as in the endpoints.

The only difference between the 2 cubic splines is that financial cubic spline 
is set to have a slope equal to zero at the endpoint to the right.


Estimation of the curvatures
============================

To estimate the curvatures one uses the first order derivatives:

.. math::
    f_{i}'(x)=\frac{(x-x_{i})^{2}\cdot k_{i+1}-(x_{i+1}-x)^{2}\cdot k_{i}}{2\cdot\triangle x_{i}}+c_{i}

And the assumption that the slopes are continous at the points 
:math:`((x_{i},y_{i}))_{i=1}^{n}`, ie

.. math::
    f_{i-1}'(x_{i}) & = f_{i}'(x_{i})\;,i=2,\ldots,n-1\\
    \Updownarrow\\
    \frac{\triangle x_{i-1}^{2}\cdot k_{i}-(x_{i}-x_{i})^{2}\cdot k_{i-1}}{2\cdot\triangle x_{i-1}}+c_{i-1} & = \frac{(x_{i}-x_{i})^{2}\cdot k_{i+1}-\triangle x_{i}^{2}\cdot k_{i}}{2\cdot\triangle x_{i}}+c_{i}\\
    \Updownarrow\\
    \frac{\triangle x_{i-1}\cdot k_{i}}{2}+\frac{\triangle y_{i-1}}{\triangle x_{i-1}}-\frac{\triangle x_{i-1}\cdot\triangle k_{i-1}}{6} & = \frac{-\triangle x_{i}\cdot k_{i}}{2}+\frac{\triangle y_{i}}{\triangle x_{i}}-\frac{\triangle x_{i}\cdot\triangle k_{i}}{6}\\
    \Updownarrow\\
    \frac{\triangle x_{i-1}\cdot k_{i}}{3}+\frac{\triangle y_{i-1}}{\triangle x_{i}}+\frac{\triangle x_{i-1}\cdot k_{i-1}}{6} & = \frac{-\triangle x_{i}\cdot k_{i}}{3}+\frac{\triangle y_{i}}{\triangle x_{i}}-\frac{\triangle x_{i}\cdot k_{i+1}}{6}\\
    \Updownarrow\\
    \frac{\triangle x_{i-1}\cdot k_{i-1}}{6}+\frac{(\triangle x_{i}+\triangle x_{i-1})\cdot k_{i}}{3}+\frac{\triangle x_{i}\cdot k_{i+1}}{6} & = \frac{\triangle y_{i}}{\triangle x_{i}}-\frac{\triangle y_{i-1}}{\triangle x_{i-1}}\\
    \Updownarrow\\
    \triangle x_{i-1}\cdot k_{i-1}+2\cdot(\triangle x_{i}+\triangle x_{i-1})\cdot k_{i}+\triangle x_{i}\cdot k_{i+1} & = 6\cdot(\frac{\triangle y_{i}}{\triangle x_{i}}-\frac{\triangle y_{i-1}}{\triangle x_{i-1}})\;,i=2,\ldots,n-1
    
This gives n - 2 linear equations to estimate n curvatures. This means that 2 asumptions are necessary in order to calculate the n curvtures.

The Natural Cubic Spline
------------------------

Here the asumptions are :math:`k_{1} = k_{n} = 0`. 
The other equations becomes :math:`i=2,\ldots,n-1`:

.. math::
    \triangle x_{i-1}\cdot k_{i-1}+2\cdot(\triangle x_{i}+\triangle x_{i-1})\cdot k_{i}+\triangle x_{i}\cdot k_{i+1} & = 6\cdot(\frac{\triangle y_{i}}{\triangle x_{i}}-\frac{\triangle y_{i-1}}{\triangle x_{i-1}})

In all n variables :math:`(k_{i})_{i=1}^n` and n linear equations

The Financial Cubic Spline
--------------------------

Here the asumptions are :math:`k_{1} = 0` and :math:`f_{n-1}'(x_{n})  = 0` or: 

.. math::
    0 & = \frac{\triangle x_{n-1}^{2}\cdot k_{n}-(x_{n}-x_{n})^{2}\cdot k_{n-1}}{2\cdot\triangle x_{n-1}}+c_{n-1}\\
      & = \frac{\triangle x_{n-1}\cdot k_{n}}{2}+\frac{\triangle y_{n-1}}{\triangle x_{n-1}}-\frac{\triangle x_{n-1}\cdot (k_{n} - k_{n-1})}{6}\\
      \Updownarrow\\
      -6\cdot \frac{\triangle y_{n-1}}{\triangle x_{n-1}} & = \triangle x_{n-1}\cdot k_{n-1} + 2\cdot \triangle x_{n-1}\cdot k_{n}

The other equations becomes :math:`i=2,\ldots,n-1`:

.. math::
    \triangle x_{i-1}\cdot k_{i-1}+2\cdot(\triangle x_{i}+\triangle x_{i-1})\cdot k_{i}+\triangle x_{i}\cdot k_{i+1} & = 6\cdot(\frac{\triangle y_{i}}{\triangle x_{i}}-\frac{\triangle y_{i-1}}{\triangle x_{i-1}})

Also here there are in all n variables :math:`(k_{i})_{i=1}^n` and n linear 
equations

