***************************************************
Using Barycentric Lagrange Interpolation in Finance
***************************************************

.. topic:: Abstract:

    Lagrange Interpolation has had a revelation since the Barycentric formulas
    were developed.
    
    Here Barycentric Lagrange Interpolation will be examined as a tool for 
    finding weights to approximate the first and second order derivative for a 
    function only known by it's functional values.
    
    The first order is used for the duration in finance while the second order 
    derivative is used for the convexity.
    
    This section is highly inspired on [BerrutTrefethen]_, [SadiqViswanath]_ 
    and [Fornberg]_. 
    
    Also as references [Kiusalaas]_ and [Ralston]_ has been invaluable.
    
Lagrange Interpolation
======================

First construct a polynomial with n + 1 different grid values 
:math:`(x_j)_{j=0 \dotsc n}`:

.. math::
    l(x) = \prod_{j=0 \dotsc n}(x - x_j)
    
This polynomial has degree n + 1. And it is obvious then that 
:math:`p(x_j) = 0 \ \forall j`.

Consider then the tangent for p in grid value :math:`x_j` , ie.:

.. math::
    y & = l'(x_j) \cdot (x - x_j) + l(x_j) \\
      & = l'(x_j) \cdot (x - x_j)
      
Now define n + 1 polynomials as:

.. math::
    l_j(x) = \frac{l(x)}{l'(x_j) \cdot (x - x_j)}
    
These polynomials all satisfy:

.. math::
    l_j(x_i) & = \begin{cases} 1 \ if \ i = j \\
                             0 \ if \ i \neq j
                 \end{cases} \\
             & = \delta_{ij}
                
In other words :math:`l_j(x_i)` is Kroenecker's delta.
Note that the :math:`l_j`'s all have degree n. 
 
Since :math:`l(x) = (x - x_j) \cdot \prod_{i \neq j} (x - x_i)` we have:
 
.. math::
    l'(x) = \prod_{i \neq j} (x - x_i) + (x - x_j) \cdot ( \prod_{i \neq j} (x - x_i) )'
    
And then when letting the grid values be the argument of l(x) (last part above 
is zero since :math:`(x_j - x_j)` is):

.. math::
    l'(x_j) & = \prod_{i \neq j} (x_j - x_i) \\
            & \neq 0
            
we have that the polynomials :math:`l_j(x)` are well defined. This is because 
:math:`(x - x_j)` can be shortened out and then the polynomials :math:`l_j(x)` 
are just scaled with a nonzero scaling constants.

.. topic:: Definition, Lagrange Interpolation and Remainder:

    Consider n + 1 points :math:`(x_j, f(x_j))_{j=0 \dotsc n}`. Then the unique    
    polynomial of the minimal degree n going through all n + 1 points is:
    
    .. math::
        p(x) = \sum_{j=0 \dotsc n} l_j(x) \cdot f(x_j)
        
    Define :math:`R(x) = f(x) - p(x)` as the Lagrange remainder.
        
Since :math:`l_j(x_i)` is Kroenecker's delta it is obvious that 
:math:`l(x_j) = l_j(x_j) \cdot f(x_j) = f(x_j)`.

.. _`Lagrange Remainder theorem`:

.. topic:: Theorem, Lagrange Remainder:

    Let :math:`]a, b[` being an open interval containing all the n+1 grid values 
    :math:`(x_j)_{j=0 \dotsc n}` and f is a n+1 times continous differentiable 
    function on :math:`]a, b[`:

    .. math::
        \forall x \in ]a, b[ \quad \exists \zeta \in ]a, b[: 
            f(x) &= \sum_{j=0 \dotsc n} l_j(x) \cdot f(x_j) 
                    + \frac{l(x) \cdot f^{(n+1)}(\zeta)}{(n+1)!} \\
                 &= p(x) + R(x)

*Proof:*
    For :math:`x \in ]a,b[`:
     
    .. math:: 
        R(x) &= f(x) - \sum_{j=0 \dotsc n} l_j(x) \cdot f(x_j) \\
             &= f(x) - p(x)
    
    Observe that :math:`f(x_j) = p(x_j) \forall j=0 \dotsc n` 
    i.e. :math:`R(x)` has n+1 zeros.
    
    Also observe that since p(x) has degree n :math:`R^{(n+1)}(x) = f^{(n+1)}(x)` 
    and :math:`p^{(n)}(x) = (n+1)!`.
    
    Now define:
    
    .. math::
        g(t) = R(t) - \frac{l(t) \cdot R(x)}{l(x)}
        
    Observe that :math:`g(t)` has both :math:`(x_j)_{j=0 \dotsc n}` and x as 
    roots, ie n+2 zeros.
    
    According to `Rolle's theorem <http://www.proofwiki.org/wiki/Rolle%27s_Theorem>`_ 
    :math:`g^{(1)}(t)` has n+1 zeros and successively :math:`g^{(2)}(t)` has n zeros.
    
    So :math:`g^{(n+1)}(t)` has 1 zero, ie 
    :math:`\exists \zeta :g^{(n+1)}(\zeta) = 0`
    or 
        
    .. math::  
        0 &= g^{(n+1)}(\zeta)\\
        0 &= R^{(n+1)}(\zeta) - \frac{(n+1)! \cdot R(x)}{l(x)} \\
        0 &= f^{(n+1)}(\zeta) - \frac{(n+1)! \cdot R(x)}{l(x)} \\
        & \Updownarrow \\
        R(x) &= \frac{l(x) \cdot f^{(n+1)}(\zeta)}{(n+1)!}
    
    (`see also <http://www.proofwiki.org/wiki/Lagrange_Polynomial_Approximation>`_)
    
    Q.E.D.
    
The Lagrange Remainder can be used to limit the overall accuracy for the Lagrange 
Interpolation if the function f(x) is n+1 times continous differentiable on the 
closed interval :math:`[a,b]`. 

This is because :math:`\exists M : |f^{(n+1)}(x)| \le M \forall x \in [a,b]`, ie:

.. math::
    |f(x) - p(x)| = |R(x)| &\le \frac{l(x) \cdot M}{(n+1)!} \\
                           &\le \frac{(b-a)^{n+1} \cdot M}{(n+1)!}
                           
So if :math:`(b-a)` converges to zero then the accuracy becomes of order n+1 ie
:math:`\mathcal{O}((b-a)^{n+1})`.

.. topic:: Observation:

    The Lagrange Interpolation can for a set of n+1 grid values 
    :math:`(x_j)_{j=0 \dotsc n}` can be seen as a way of interpolating a function 
    value f(x) based on a weighted average of the functional values 
    :math:`(f(x_j))_{j=0 \dotsc n}` where the weights are 
    :math:`(l_j(x))_{j=0 \dotsc n}` and the accuracy will be of order 
    :math:`\mathcal{O}((b-a)^{n+1})`.
    
    This can be taken even further. Since only the weights depends on x the 
    first order differentiation of :math:`f(x),\ f^{(1)}(x)` can be approximated 
    by:
    
    .. math::    
        p^{(1)}(x) = \sum_{j=0 \dotsc n} l_j^{(1)}(x) \cdot f(x_j)
        
    So the first order derivative :math:`f^{(1)}(x)` at x can be approximated by 
    the functional values :math:`(f(x_j))_{j=0 \dotsc n}` and the weights are 
    :math:`(l_j^{(1)}(x))_{j=0 \dotsc n}` and the accuracy will be of order 
    :math:`\mathcal{O}((b-a)^{n})`, ie the accuracy loses 1 degree.
    
    And successively for the second order derivative:
    
    .. math::    
        p^{(2)}(x) = \sum_{j=0 \dotsc n} l_j^{(2)}(x) \cdot f(x_j) 
                    + \mathcal{O}((b-a)^{n-1})
        
    This makes Lagrange Interpolation well suited for finding weighted averages 
    of functional values :math:`(f(x_j))_{j=0 \dotsc n}` for derivatives of any 
    order with high accuracy, 
    :math:`\mathcal{O}((b-a)^{number\ of\ points - degree\ of\ differentiation})`.
    
Barycentric Lagrange Interpolation
==================================

Computationally Lagrange Interpolation isn't that effective. But further 
analysis shows that the :math:`p(x_j)`'s all have the same common factor 
:math:`\prod_{j=0 \dotsc n}(x - x_j)` so we have the **First Barycentric formula**:

.. math::
   :label: 1Barycentric

    p(x) &= \sum_{j=0 \dotsc n} l_j(x) \cdot f(x_j) \\
         &= \prod_{j=0 \dotsc n}(x - x_j) \cdot \sum_{j=0 \dotsc n} \frac{w_j}{x - x_j} \cdot f(x_j) \\
         &= l(x) \cdot \sum_{j=0 \dotsc n} \frac{w_j}{x - x_j} \cdot f(x_j) 
             
where

.. math::
    w_j &= \frac{1}{l'(x_j)} \\
        &= \frac{1}{\prod_{i \neq j} (x_j - x_i)}
           
Looking at the constant function 1 gives:

.. math::
    1 = l(x) \cdot \sum_{j=0 \dotsc n} \frac{w_j}{x - x_j}
    
or 

.. math::
    l(x) = \frac{1}{\sum_{j=0 \dotsc n} \frac{w_j}{x - x_j}}
    
This way we have the **Second Barycentric formula**:

.. math::
   :label: 2Barycentric

    p(x) = \frac{\sum_{j=0 \dotsc n} \frac{w_j}{x - x_j} \cdot f(x_j)}{\sum_{j=0 \dotsc n} \frac{w_j}{x - x_j}}

Both formulas shows a remarkable low dependence on the :math:`f(x_j)`'s since a change in one :math:`f(x_j)`'s will have a multiplicative effect only one place.

Algorithms
----------

Interpolation should be done based on the Second Barycentric formula 
:eq:`2Barycentric`.

For updating the weights with an extra point consider a set of basis points 
:math:`(x_j, f(x_j))_{j=0 \dotsc n}` to be used as a basis for Barycentric 
Lagrange interpolation.

When a new grid value :math:`x_{n+1}` has to be added the algorithm is:

    #. Calculate :math:`w_j := \frac{w_j}{x_j - x_{n+1}}\forall j`    
    #. Calculate :math:`w_{n+1} := \frac{1}{\prod_{j=0 \dotsc n+1}(x_{n+1} - x_j)}`

It is obvious that there needs to be minimum 2 values. When there are only 2 grid values :math:`x_1` and :math:`x_2` then :math:`w_1 = -w_2`.

Numerical differentiation at grid points
----------------------------------------

One of the things to use Barycentric Lagrange interpolation is to evaluate is to differentiate a curve at a point when only a set points are known with a high 
precision.

Usual formulas are eg, :math:`\frac{f(x + h) - f(x - h)}{2 \cdot h}` which is 
considered as a 3 point estimation based on the points with first coordinate 
(x - h, x, x + h).

The precision or accuracy is here of order 2.

Now consider:

.. math::
    f(x) & = \sum_{j=0 \dotsc n} l_j(x) \cdot f(x_j) \\
    & \Updownarrow \\
    f^{(1)}(x) & = \sum_{j=0 \dotsc n} l^{(1)}_j(x) \cdot f(x_j) 
    

Now according to Second Barycentric formula :eq:`2Barycentric`:

.. math::
    l_j(x) &= \frac{\frac{w_j}{x - x_j}}{\sum_{k=0 \dotsc n} \frac{w_k}{x - x_k}} \\
    & \Updownarrow \\
     l_j(x) \cdot \sum_{k=0 \dotsc n} \frac{w_k}{x - x_k} &= \frac{w_j}{x - x_j}
     
Letting :math:`s_i(x) = \sum_{k=0 \dotsc n} \frac{w_k \cdot (x - x_i)}{x - x_k}` where 
:math:`x_i` is one of the roots :math:`(x_k)_{k=0 \dotsc n}` (Important 
assumption) we have:

.. math::
    :nowrap:
        
    \begin{eqnarray}
     l_j(x) \cdot s_i(x) &=& \frac{w_j \cdot (x - x_i)}{x - x_j} \nonumber \\ 
     & \Downarrow \nonumber \\
     l_j^{(1)}(x) \cdot s_i(x) + l_j(x) \cdot s_i^{(1)}(x) &=& \frac{(x - x_j) - (x - x_i)}{(x - x_j)^2} \cdot w_j \nonumber \\
     & \Updownarrow \nonumber \\
     l_j^{(1)}(x) \cdot s_i(x) + l_j(x) \cdot s_i^{(1)}(x) &=& \frac{x_i - x_j}{(x - x_j)^2} \cdot w_j \nonumber  
    \end{eqnarray}

The last equation will be used several times so it gets a number:

.. math::
    :label: BarycentricFirstOrderBase

    l_j^{(1)}(x) \cdot s_i(x) + l_j(x) \cdot s_i^{(1)}(x) = \frac{x_i - x_j}{(x - x_j)^2} \cdot w_j

Since by design :math:`s_i(x_i) = w_i` and :math:`l_j(x_i) = \delta_{ij}` we have for :math:`i \neq j` in :eq:`BarycentricFirstOrderBase`:

.. math::
     l_j^{(1)}(x_ i) \cdot s_i(x_i) + l_j(x_i) \cdot s_i^{(1)}(x_i) &= \frac{1}{x_i - x_j} \cdot w_j \\
     & \Updownarrow \\
     l_j^{(1)}(x_i) \cdot w_i &= \frac{1}{x_i - x_j} \cdot w_j \\
     & \Updownarrow \\
     l_j^{(1)}(x_i) &= \frac{w_j}{(x_i - x_j) \cdot w_i}
     
Now for i = j we have:

.. math::
    1 &= \sum_{k=0 \dotsc n} l_k(x) \\
     & \Downarrow \\
    0 &= \sum_{k=0 \dotsc n} l^{(1)}_k(x) \\
     & \Downarrow \\
    0 &= \sum_{k=0 \dotsc n} l^{(1)}_k(x_j) \\
     & \Updownarrow \\
     l^{(1)}_j(x_j) &= - \sum_{k \neq j} l^{(1)}_k(x_j)

This way we have:

.. _`Barycentric first order derivative`:

.. topic:: Theorem, Barycentric first order derivative:

    Consider a set of grid values :math:`(x_i)_{i=0 \dotsc n}` and the function values 
    :math:`(f(x_i))_{i=0 \dotsc n} = (f_i)_{i=0 \dotsc n}`.
    The formula for the first order derivative at one of the grid values 
    :math:`x_i`, :math:`f^{(1)}(x_i)` is:

    .. math::
      f^{(1)}(x_i) = \sum_{j=0 \dotsc n} l^{(1)}_j(x_i) \cdot f(x_j)  

    where 

    .. math::
        l_j^{(1)}(x_i) &= \frac{w_j}{(x_i - x_j) \cdot w_i} \ for \  i \neq j \\
        l^{(1)}_i(x_i) &= - \sum_{k \neq i} l^{(1)}_k(x_i) \ for \  i = j

.. _`Lagrange Example 1`:

.. topic:: Example 1:

    The formula for interpolating the first order derivative can used to
    optimize the approximation of the derivative.
    Consider eg :math:`(x_i)_{i = 0 \dotsc 1}`. Then 
    :math:`(w_i)_{i = 0 \dotsc 1} = (\frac{1}{x_1 - x_0}, \frac{-1}{x_1 - x_0})` 
    and:
    
    .. math::
        l^{(1)}_j(x_i) &= \left( \begin{matrix} 
                        \frac{1}{x_0 - x_1}  & \frac{-1}{x_0 - x_1} \\
                        \frac{1}{x_0 - x_1} & \frac{-1}{x_0 - x_1} 
                     \end{matrix} \right) \\
                   &= \frac{1}{x_0 - x_1} \cdot 
                     \left( \begin{matrix} 
                        1  & -1 \\
                        1 & -1 
                     \end{matrix} \right)

    where i is the row index while j is the column index.

    And then with:

    .. math::
        \left( \begin{matrix} f^{(1)}(x_0) \\ f^{(1)}(x_1) \end{matrix} \right) = l^{(1)}_j(x_i) \cdot \left( \begin{matrix} f(x_0) \\ f(x_1) \end{matrix} \right)

    
    The result is:

    .. math::
        f^{(1)}(x_0) = f^{(1)}(x_1) = \frac{f(x_0) - f(x_1)}{x_0 - x_1} + \mathcal{O}(|x_0 -x_1|)

    And this is the formula for Newton's difference quotient.
    
.. _`Lagrange Example 2`:

.. topic:: Example 2:

    Consider eg :math:`(x_i)_{i = 0 \dotsc 2} = (x - h, x, x + h)`. Then :math:`(w_i)_{i = 0 \dotsc 2} = (\frac{1}{2h^2}, \frac{1}{-h^2}, \frac{1}{2h^2})` and:

    .. math::
        l^{(1)}_j(x_i) &= \left( \begin{matrix} 
                       - \sum_{k \neq (x - h)} l^{(1)}_k(x - h) & \frac{2h^2}{-h^2 \cdot ((x - h) - x)} & \frac{2h^2}{2h^2 \cdot ((x - h) -(x + h))} \\
                       \frac{-h^2}{2h^2 \cdot (x - (x - h))} & - \sum_{k \neq x} l^{(1)}_k(x) & \frac{-h^2}{2h^2 \cdot (x - (x + h))} \\
                       \frac{2h^2}{2h^2 \cdot ((x + h) - (x - h))} & \frac{2h^2}{-h^2 \cdot ((x + h) - x)} & - \sum_{k \neq (x + h)} l^{(1)}_k(x + h)  
                    \end{matrix} \right) \\
                  &= \frac{1}{h} \cdot \left( \begin{matrix} 
                       -1 \frac{1}{2} & 2 & \frac{-1}{2} \\
                       \frac{-1}{2} & 0 & \frac{1}{2} \\
                       \frac{1}{2} & -2 & 1 \frac{1}{2}  
                    \end{matrix} \right)

    These coefficients are to formula 25.3.4 on page 883 in [AbramowitzStegun]_.
    
    They are all of accuracy :math:`\mathcal{O}(h^{2})`.
    
    Looking at the middle row the formula for Newton's difference quotient is found 
    once again:
    
    .. math::
    
        f^{(1)}(x) &= \frac{\frac{-1}{2} \cdot f(x - h) + \frac{1}{2} \cdot f(x + h)}{h} \\
              &= \frac{f(x + h) - f(x - h)}{(x + h) - (x - h)} \\
              &= \frac{f(x + h) - f(x - h)}{2 \cdot h}


Differentiating :eq:`BarycentricFirstOrderBase` leads to:

.. math::
    :label: BarycentricFirstOrderBase2

    l_j^{(2)}(x) \cdot s_i(x) + 2 \cdot l_j^{(1)}(x) \cdot s_i^{(1)}(x) + l_j(x) \cdot s_i^{(2)}(x) = -2 \cdot \frac{x_i - x_j}{(x - x_j)^3} \cdot w_j

Again by design :math:`s_i(x_i) = w_i` and :math:`l_j(x_i) = \delta_{ij}` we have for :math:`i \neq j` in :eq:`BarycentricFirstOrderBase2`:

.. math::

    l_j^{(2)}(x_i) \cdot s_i(x_i) + 2 \cdot l_j^{(1)}(x_i) \cdot s_i^{(1)}(x_i) + l_j(x_i) \cdot s_i^{(2)}(x_i) &= \frac{-2 \cdot w_j}{(x_i - x_j)^2} \\
    \Updownarrow \\
    l_j^{(2)}(x_i) \cdot w_i + 2 \cdot l_j^{(1)}(x_i) \cdot s_i^{(1)}(x_i) &= \frac{-2 \cdot w_j}{(x_i - x_j)^2}
    
From Barycentric first order derivative `Barycentric first order derivative`_ we have 
:math:`l_j^{(1)}(x_i) = \frac{w_j}{(x_i - x_j) \cdot w_i} \ for \  i \neq j` so:

.. math::

    l_j^{(2)}(x_i) \cdot w_i + 2 \cdot \frac{w_j}{(x_i - x_j) \cdot w_i} \cdot s_i^{(1)}(x_i) = \frac{-2 \cdot w_j}{(x_i - x_j)^2}
    
    
Finally since :math:`s_i^{(1)}(x) = \sum_{k \neq i} w_k \cdot \frac{(x - x_k) - (x - x_i)}{(x - x_k)^2} = \sum_{k \neq i} w_k \cdot \frac{(x_i - x_k)}{(x - x_k)^2}`:

.. math::

    l_j^{(2)}(x_i) \cdot w_i + 2 \cdot \frac{w_j}{(x_i - x_j) \cdot w_i} \cdot \sum_{k \neq i} \frac{w_k}{x - x_k} &= \frac{-2 \cdot w_j}{(x_i - x_j)^2} \\
    & \Updownarrow \\
    l_j^{(2)}(x_i) \cdot w_i &= \frac{-2 \cdot w_j}{(x_i - x_j)^2} - 2 \cdot \frac{w_j}{(x_i - x_j) \cdot w_i} \cdot \sum_{k \neq i} \frac{w_k}{x_i - x_k} \\
    & \Updownarrow \\
    l_j^{(2)}(x_i) &= \frac{ - 2 \cdot w_j}{(x_i - x_j) \cdot w_i} \cdot \left(\frac{1}{x_i - x_j} + \sum_{k \neq i} \frac{w_k}{(x_i - x_k) \cdot w_i} \right) \\
    & \Updownarrow \\
    l_j^{(2)}(x_i) &= \frac{ - 2 \cdot w_j}{(x_i - x_j) \cdot w_i} \cdot \left(\frac{1}{x_i - x_j} + l_i^{(1)}(x_i) \right)

Now for i = j we have something similar as for the first order case:

.. math::
    1 &= \sum_{k=0 \dotsc n} l_k(x) \\
     & \Downarrow \\
    0 &= \sum_{k=0 \dotsc n} l^{(2)}_k(x_j) \\
     & \Updownarrow \\
     l^{(2)}_j(x_j) &= - \sum_{k \neq j} l^{(2)}_k(x_j)

This way we have:

.. _`Barycentric second order derivative`:

.. topic:: Theorem, Barycentric second order derivative:

    Consider a set of grid values :math:`(x_i)_{i=0 \dotsc n}` and the function values 
    :math:`(f(x_i))_{i=0 \dotsc n} = (f_i)_{i=0 \dotsc n}`.
    The formula for the second order derivative at one of the grid values 
    :math:`x_i`, :math:`f^{(1)}(x_i)` is:

    .. math::
      f^{(2)}(x_i) = \sum_{j=0 \dotsc n} l^{(2)}_j(x_i) \cdot f(x_j)  

    where 

    .. math::
        l_j^{(2)}(x_i) &= \frac{ - 2 \cdot w_j}{(x_i - x_j) \cdot w_i} \cdot \left(\frac{1}{x_i - x_j} + l_i^{(1)}(x_i) \right) \ for \  i \neq j \\
        l^{(2)}_i(x_i) &= - \sum_{k \neq i} l^{(2)}_k(x_i) \ for \  i = j

.. note:: There is a sign error in [BerrutTrefethen]_ page 513

    
.. _`Lagrange Example 2, second order`:

.. topic:: Example 2, second order:

    Consider eg :math:`(x_i)_{i = 0 \dotsc 2} = (x - h, x, x + h)`. Then 
    :math:`(w_i)_{i = 0 \dotsc 2} = (\frac{1}{2h^2}, \frac{1}{-h^2}, \frac{1}{2h^2})` 
    and :math:`l_x^{(1)}(x) = 0` so:

    .. math::
        l^{(2)}_j(x) &= \left( \begin{matrix} 
                        \frac{1}{h^2}, & \frac{-2}{h^2}, & \frac{1}{h^2} \\
                        \end{matrix} \right)
                        
    This is usual Newton second order central finite difference approximation of 
    accuracy :math:`\mathcal{O}(h)`, ie:

    .. math::
      f^{(2)}(x) = \frac{f(x-h) - 2 \cdot f(x) + f(x+h)}{h^2}

    Also :math:`l_{x-h}^{(1)}(x-h) = \frac{-3}{2 \cdot h}` so:

    .. math::
        l^{(2)}_j(x-h) &= \left( \begin{matrix} 
                        - \sum_{k \neq i} l^{(2)}_k(x_i), 
                        & \frac{ - 2 \cdot \frac{1}{-h^2}}{h \cdot \frac{1}{2h^2}} 
                                \cdot \left(\frac{1}{h} + \frac{-3}{2 \cdot h} \right), 
                        & \frac{ - 2 \cdot \frac{1}{2h^2}}{h \cdot \frac{1}{2h^2}} 
                                \cdot \left(\frac{1}{2 \cdot h} + \frac{-3}{2 \cdot h} \right) \\
                        \end{matrix} \right) \\
                     &= \left( \begin{matrix} 
                        - \sum_{k \neq i} l^{(2)}_k(x_i), 
                        & \frac{4}{h} \cdot \frac{-1}{2 \cdot h}, 
                        & \frac{-2}{h} \cdot \frac{-1}{2 \cdot h} \\
                        \end{matrix} \right) \\
                     &= \left( \begin{matrix} 
                        \frac{1}{h^2}, 
                        & \frac{-2}{h^2}, 
                        & \frac{1}{h^2} \\
                        \end{matrix} \right)

    This is usual Newton second order forward finite difference approximation of 
    accuracy :math:`\mathcal{O}(h)`, ie:

    .. math::
      f^{(2)}(x - h) = \frac{f(x-h) - 2 \cdot f(x) + f(x+h)}{h^2}

    Both results are in accordance with table 25.2 page 914 in [AbramowitzStegun]_.
    
[SadiqViswanath]_ study the fact that the accuracy sometimes is 1 higher than 
specified by Lagrange, ie the accuracy is boosted. One of their results are:

.. topic:: Corollary 7 page 14:

    Look at the grid values :math:`(x_i)_{i=0 \dotsc n} = (h \cdot z_i)_{i=0 \dotsc n}`.
    If the grid relative grid values :math:`(z_i)_{i=0 \dotsc n}` are symmetric 
    about 0 (in other words z is a relative grid value if and only if −z is a 
    relative grid value) and n − m (m is the order of the derivative) is odd, 
    the order of accuracy is boosted by 1.

So according to the corollary the Newton second order central finite difference 
approximation is actually of accuracy :math:`\mathcal{O}(h^2)` since
:math:`(x_i)_{i = 0 \dotsc 2} = (x - h, x, x + h)` has symmetric relative grid 
values and :math:`n - m = 3 - 1 = 1` is odd.
On the other hand the accuracy of the Newton second order forward finite 
difference approximation is still :math:`\mathcal{O}(h)`.
This is confirmed by table 25.2 page 914 in [AbramowitzStegun]_.

.. topic:: Decision, the finance package:

    In the finance package the derivatives numericcally will approximated such 
    that the accuracy :math:`\mathcal{O}(0.0001^4)` leading to an accuracy down 
    to the sixteenth decimal.
