*****************************************************************
Using LaTeX syntax for mathematics in RestructuredText and Sphinx
*****************************************************************

.. topic:: Abstract:

    This document is about using Latex in reStructuredText and Sphinx.
    At the link below there is a introduction to mathematical symbols in Latex. 

    ftp://ftp.ams.org/pub/tex/doc/amsmath/short-math-guide.pdf

Math/Latex in RestructuredText and Sphinx
=========================================

There are 2 ways of writing math in restructuredText:

**Inline ``:math:`SomeLatex```**
    To insert relatively simple latex for mathematical expressions like 
    :math:`\psi(r)=\exp(-2r)` in the text write ``:math:`\psi(r) = \exp(-2r)```
    Inside the back-tics (`) any Latex code can be written. There must be no
    space between :math: and the first back-tic.

**Complex Latex like eg equation**
    For producing more complex math like eg an ``equation*`` environment in a
    LaTeX document write::
    
        .. math::
            \psi(r) = e^{-2r}

    you will get:
    
    .. math::
        \psi(r) = e^{-2r}


.. warning::
    The math markup can be used within reST files (to be parsed by Sphinx) 
    but within your python’s docstring, the slashes need to be escaped ! 
    ``:math:`\alpha``` should therefore be written ``:math:`\\alpha``` or put 
    an “r” before the docstring

Examples
========

Below are some examples of Latex commands and symbols:

To do square roots like eg :math:`\sqrt{x^2-1}` use ``:math:`\sqrt{x^2-1}```. 

To do fractions like eg :math:`\frac{1}{2}` use ``:math:`\frac{1}{2}```. 

In order to insert text into a formula use ``:math:`k_{\text{B}}T``` to get 
like in :math:`k_{\text{B}}T`

Use ``\left`` and ``\right`` for before brackets like in 
``:math:`\left(\frac{1}{2}\right)^n``` to get :math:`\left(\frac{1}{2}\right)^n`.

Displayed math can use ``\\`` and ``&`` for line shifts and
alignments, respectively. ::

    .. math::
      a & = (x + y)^2 \\
        & = x^2 + 2xy + y^2

The result is:

.. math::
   a & = (x + y)^2 \\
     & = x^2 + 2xy + y^2

The ``matrix`` environment can also contain ``\\`` and ``&``.
To get like:

.. math::
  \left(\begin{matrix} a & b \\ c & d \end{matrix}\right)

write: ::

    .. math::
      \left(\begin{matrix} a & b \\
        c & d \end{matrix}\right)

Equations are labeled with the ``label`` option and referred to using: :: 

    :eq:`label`

E.g:

.. math:: 
   :label: pythag

    a^2 + b^2 = c^2

See equation :eq:`pythag` 

is written like: ::

    .. math:: 
       :label: pythag

        a^2 + b^2 = c^2

    See equation :eq:`pythag` 

Note that:

    #. No spaces in label name
    #. There should be a blank line before the actual latex code
    #. There should be a space between :label: and the label name
    #. When referred to label name should be surrounded by `
    
