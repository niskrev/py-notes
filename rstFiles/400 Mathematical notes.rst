##################
Mathematical notes
##################

Here mathematical notes are kept.

.. contents::

.. include:: 400_Mathematical_notes/latex_math.rst

.. include:: 400_Mathematical_notes/SplineAtScipy.rst

.. include:: 400_Mathematical_notes/cubicSplineDerivation.rst

.. include:: 400_Mathematical_notes/BarycentricLagrangeInterpolation.rst

.. include:: 400_Mathematical_notes/GaryFosheeTuesdayProblem.rst
