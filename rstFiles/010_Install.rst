##############
How to install
##############

***************
Package finance
***************

Download and install. It is applicable to all version of python on version 2.5,
2.6 and 2.7.
The code will soon be prepared for the version 3.

:download: `finance at pypi <http://pypi.python.org/pypi/finance>`_

Requirements:
=============

There are no requirements.

*****************
Package decimalpy
*****************

Download and install. It is applicable to all version of python on version 2.5,
2.6 and 2.7.
The code will soon be prepared for the version 3.

:download: `decimalpy at pypi <http://pypi.python.org/pypi/decimalpy>`_

Requirements:
=============

There are no requirements.
