**********************************************************
An example of Method and operator overload in spreadsheets
**********************************************************

.. topic:: Abstract:

	In spreadsheets like [KSpread]_, [GNumeric]_ or the Open Office [Calc]_ 
	dates are handled the following.

		#. You type in a date in a date-specific format
		#. The spreadsheet saves the date in the cell as an integer, 
		   but keep showing the date like the format

	This way a date is defined as an integer being the number of days since 
	1899-12-31 (date = 0).
	This is inspired by the way it is done in the overall spreadsheet benchmark 
	[msExcel]_.

	The benefit is that it is easy to a number of days to a date or finding 
	the number of days between 2 dates as a difference.

	This chapter is about how this way of adding functionality to a new class 
	is generalized in Python.
	And showing that spreadsheets might benefit from this sort of generalisations.

.. contents::

An example:
===========

The picture shows how dates are represented in spreadsheets in order to simplify 
calculations.

.. image:: 500_Notes_on_spreadsheets_etc/pictures/MethodAndOperatorOverload_GNumeric.png
   :align: center
   
The formulas in column A is shown in column B. This way both values and 
formulas are shown.

* First a date is entered in A1. Even though it is saved as and integer 
  (see 40544 in cell B1) it is shown as a date.
* Next in A2 the formula "=A1+55" (see B2) is entered. The date format is inherited 
  from A1 and the date 55 days ahead is shown. Here a number of days are added to 
  a date.
* Finally in A3 the difference between the 2 dates is calculated by the formula 
  "=A2-A1" (see B3). The result is surpricingly enough 55 days.

Note how the format defines appearance while the fact that a date is actually is an integer 
makes it possible to do calculations on dates similar to *operator overload* which is to be 
presented below.

The change of format on the other hand is in fact a case of *method overload* which also is to be 
presented below.

The functionality of dates in spreadsheets is mirrored in the class bankdate in the finance 
package.

This very intuitive way of working with dates and period we would like to transfer to Python.

How things are done in Python
=============================

Some concepts from the finance module
-------------------------------------

To examplify I will introduce some concepts from the finance module.

timeperiod:
	A timeperiod is in essence a number and a time unit (d(ays), m(onths), y(ears)).

You can do timeperiod calculations within the same time unit:

>>> from finance import TimePeriod
>>> 5 * TimePeriod('4m') - 3
17m

bankdate:
	A bankdate shows a date as string in the format 'yyyy-mm-dd'.

You can a bankdate with one or more timeperiods and get a new bankdate.
And you can subtract 2 bankdates and get the number of days inbetween.
Note that no value at instantion means that bankdate becomes current date.

>>> from finance import BankDate
>>> BankDate('2010-09-14')  # No value set means today
2010-09-14
>>> BankDate('2010-09-14') + '3m'
2010-12-14
>>> BankDate('2010-09-14') + '3m' + '2y'
2012-12-14
>>> BankDate('2010-09-14') + '3m' - BankDate('2010-09-14')
91

Inheritance and method overload is one of the cornerstones object oriented programming.
Below it is shown used to ease coding with these mathematically inspired objects.

Operator and method overload
----------------------------

In python it is easy to set up eg bankdate and timeperiod calculations.
In every class definition it is possible to define internal methods like
__add__ and __radd__ for left and right addition.
The methods are used when an instance of this class is added something
in the code.

In Python there are also some standard methods used for presentation:

__str__: 
	Method defining what is shown when print instances of a class.
	It also defines what is to be shown when eg the str function
	is used on a instances of the class

__repr__:
	Method defining what is shown when an instance is entered at the
	command promt and the repr function

A lot of times these 2 methods are set to the same. But there is no reason
not to shown the values of the properties in eg __repr__ and some modified
version in the __str__./reference/datamodel.html

For further reading see eg `Python <http://www.python.org/reference/datamodel.html>`_

A suggestion for spreadsheet development
========================================

As it has been demonstrated spreadsheets does have the need for defining new 
datatypes and new definitions for one or more operator.

It would be nice if the set of standard class functions were available in the 
scripting within spreadsheets.

This way it would fairly easy to implement mathematical objects like complex
numbers, vectors and matrices.

Here the operator definitions are clear. But it could be nice if it was possible 
to show different information at different times, eg to show a matrix by its
values some times and sometimes to show the dimensions of the matrix.

Another line of objects could be objects to build binomial trees, Monte Carlo
paths etc.

This is a bit like a formula in spreadsheets. It shows a value most of the time.
But in a certain mode the formula is shown instead.

A suggestion for Python development
===================================

A further development in Python could be generic operator overload. Why is the
developer limited to specified number of operators.
In order to implement new operators it would be nice to specify a operator in 
the code.
The need for new operators could be inspired from mathematical concepts like 
eg differentiation, summation and integration.

If code objects could more be similar to mathematical objects it create a 
integration between programming and applied mathematics which yet has to be 
seen.

It would affect both education and the use of applied mathematics when the
writing language are almost the same in theory and in practise.  
