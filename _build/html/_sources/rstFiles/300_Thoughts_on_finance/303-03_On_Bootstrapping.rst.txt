On Bootstrapping
================

    >>> times = [0.5, 1, 2, 4, 5, 10, 15, 20]
    >>> rates = [0.0552, 0.06, 0.0682, 0.0801, 0.0843, 0.0931, 0.0912, 0.0857]
    

    >>> import finance
    >>> f1 = finance.yieldcurves.NaturalCubicSpline(times, rates)

    >>> f2 = finance.yieldcurves.PiecewiseConstant(times, rates)


    >>> import matplotlib.pyplot as plt
    >>> xvalues = [i * 0.05 for i in range(0, 601)]
    >>> plt.plot(times, rates, 'o', 
    ...          xvalues, f1.continous_forward_rate(xvalues), '-', 
    ...          xvalues, f2.continous_forward_rate(xvalues), '-'
    ...         )  # doctest: +SKIP
    >>> plt.legend(['data', 
    ...             'natural continous_forward_rate', 
    ...             'piecewise constant continous_forward_rate'
    ...             ],
    ...           loc='best'
    ...         )  # doctest: +SKIP
    >>> plt.show()


.. image:: 300_Thoughts_on_finance/pictures/piecewise_linear_and_a_natural_spline.png
   :align: center
   

