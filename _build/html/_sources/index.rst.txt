.. Python Hacks documentation master file, created by
   sphinx-quickstart on Sun Dec 19 22:01:31 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. header:: For comments please contact `Niels Henrik Bruun <mailto:niels.henrik.bruun@gmail.com?subject=Python%20Hacks%20comment>`_

########################
Welcome to Python Hacks!
########################

:Date: |today| by `Niels Henrik Bruun <mailto:niels.henrik.bruun@gmail.com?subject=Python%20Hacks%20comment>`_

The purpose is to develop and gather python based object oriented effective code
to work with financial/scientific calculations like risk management, pricing and hedging.
Further the purpose to get or produce proper financial documentation for the scripts
presented here.

I have found that Python is a fantastic tool to develop financial tools.
Some of the reasons for this are:

    * Python is fully object oriented
    * Python is strongly supported on numerical calculations
    * Python is easy to learn and have a clear syntax
    * Python is a worthy alternative to eg. Excel and Matlab doing Scientific calculations
    * Python can be almost as fast as eg. C or Fortran due to the fact that slow code
      parts are converted to C or Fortran module and run from Python
    * `See more arguments for using Python <http://docs.python.org/howto/advocacy>`_

But allthough the financial system Front Arena use Python as a scripting language,
there are very few homepages dedicated to financial calculations in Python.
As Python is overlooked in the financial community I decided to start this site.

This is not just about Python, though. Spreadsheets is also looked upon with regard to
scientific/financial calculations and scripting.

If you got comments to this site please send me a mail.

  Regards

  `Niels Henrik Bruun <mailto:niels.henrik.bruun@gmail.com?subject=Python%20Hacks%20comment>`_


Contents:
=========

.. toctree::
   :maxdepth: 2
   :numbered:
   :glob:

   rstFiles/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
