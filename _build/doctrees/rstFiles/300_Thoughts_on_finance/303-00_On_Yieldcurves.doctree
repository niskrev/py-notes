��a      �docutils.nodes��document���)}���}�(�refnames�}��refids�}��	nametypes�}�(�on yieldcurves�N�$addding a parallel shift or a spread�N�zero_coupon_rate�N�instantanious_forward_rate�N�continous_forward_rate�N�discount_factor�N� the basic design for yieldcurves�N�discrete_forward_rate�N�continous_rate_timeslope�N�basic concepts�Nu�substitution_defs�}��indirect_targets�]��autofootnotes�]��autofootnote_start�K�
decoration�N�symbol_footnote_refs�]��autofootnote_refs�]��symbol_footnote_start�K �substitution_names�}��	footnotes�]��current_source�N�ids�}�(�zero-coupon-rate�h �section���)}���}�(hh�parent�h,)}���}�(hhh0h,)}���}�(hhh0h�
attributes�}�(�names�]�ha�backrefs�]��dupnames�]��ids�]��on-yieldcurves�a�classes�]�u�source���/Users/BadWizard/Documents/Projects/ECB-2017/documentation/py-notes/py-notes/rstFiles/300_Thoughts_on_finance/303-00_On_Yieldcurves.rst��	rawsource�� ��tagname�h+�line�K�children�]�(h �title���)}���}�(hhh0h5h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�On Yieldcurves�hHhLhIKhJ]�h �Text����On Yieldcurves���}���}�(h0hOhFhWubaubh �topic���)}���}�(hhh0h5h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhFXQ  In the presentation on Time and value the key subject was unique price
function. Instead of talking of prices of the cashflows it is customary to
talk of the price of lending, i.e. the rate or yield.
Here we will look into several different types of yield curve definitions.
These yield curves will be implemented in the finance package.�hHh`hINhJ]�(hM)}���}�(h0hch7}�(h9]�h;]�h=]�h?]�hB]�uhF�	Abstract:�hHhLhJ]�hZ�	Abstract:���}���}�(h0hnhFhvubaubh �	paragraph���)}���}�(h0hch7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhFXQ  In the presentation on Time and value the key subject was unique price
function. Instead of talking of prices of the cashflows it is customary to
talk of the price of lending, i.e. the rate or yield.
Here we will look into several different types of yield curve definitions.
These yield curves will be implemented in the finance package.�hHh}hIKhJ]�hZXQ  In the presentation on Time and value the key subject was unique price
function. Instead of talking of prices of the cashflows it is customary to
talk of the price of lending, i.e. the rate or yield.
Here we will look into several different types of yield curve definitions.
These yield curves will be implemented in the finance package.���}���}�(h0h�hFh�ubaubeubh,)}���}�(hhh0h5h7}�(h9]�hah;]�h=]�h?]��basic-concepts�ahB]�uhDhEhFhGhHh+hIKhJ]�(hM)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�Basic concepts�hHhLhIKhJ]�hZ�Basic concepts���}���}�(h0h�hFh�ubaubh �note���)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�5There no implementation of calibration at this point.�hHh�hINhJ]�h~)}���}�(h0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhFh�hHh}hIKhJ]�hZ�5There no implementation of calibration at this point.���}���}�(h0h�hFh�ubaubaubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�`Since most yieldcurve models are specified as a sum of one or more simple yield
curve functions.�hHh}hIKhJ]�hZ�`Since most yieldcurve models are specified as a sum of one or more simple yield
curve functions.���}���}�(h0h�hFh�ubaubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�~A yieldcurve function must be a callable class. Also a string representation
must be defined showing name and used parameters.�hHh}hIKhJ]�hZ�~A yieldcurve function must be a callable class. Also a string representation
must be defined showing name and used parameters.���}���}�(h0h�hFh�ubaubeubh2h,)}���}�(hhh0h5h7}�(h9]�hah;]�h=]�h?]��$addding-a-parallel-shift-or-a-spread�ahB]�uhDhEhFhGhHh+hIK�hJ]�(hM)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�$Addding a Parallel Shift or a spread�hHhLhIK�hJ]�hZ�$Addding a Parallel Shift or a spread���}���}�(h0h�hFh�ubaubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��Since quotes typically are for the zero_coupon_rate then it makes most sence to
define a parallel shift, additive_shift to the zero_coupon_rate giving:�hHh}hIK�hJ]�hZ��Since quotes typically are for the zero_coupon_rate then it makes most sence to
define a parallel shift, additive_shift to the zero_coupon_rate giving:���}���}�(h0j  hFj
  ubaub�sphinx.ext.mathbase��displaymath���)}���}�(hhh0h�h7}�(�nowrap��h9]�h=]��latex��icontinous\_forward\_rate(t, additive\_shift) =
\ln\left(1 + zero\_coupon\_rate + additive\_shift\right)

�h;]��number�N�docname��6rstFiles/300_Thoughts_on_finance/303-00_On_Yieldcurves�h?]�hB]��label�NuhDhEhFhGhHj  hIK�hJ]�ubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhFX�  On the other the uses of shift often requires one to find the shift that gives a
cashflow a certain target value.
In that case it is more feasable to add a shift to the continous_forward_rate.
**This type will be called a multiplicative shift**. It is worth noting that in
many textbooks and articles where the authors uses a continous compounding then
a shift will be multiplicative.�hHh}hIK�hJ]�(hZ��On the other the uses of shift often requires one to find the shift that gives a
cashflow a certain target value.
In that case it is more feasable to add a shift to the continous_forward_rate.
���}���}�(h0j&  hF��On the other the uses of shift often requires one to find the shift that gives a
cashflow a certain target value.
In that case it is more feasable to add a shift to the continous_forward_rate.
�ubh �strong���)}���}�(h0j&  h7}�(h9]�h;]�h=]�h?]�hB]�uhF�3**This type will be called a multiplicative shift**�hHj6  hJ]�hZ�/This type will be called a multiplicative shift���}���}�(h0j9  hFhGubaubhZ��. It is worth noting that in
many textbooks and articles where the authors uses a continous compounding then
a shift will be multiplicative.���}���}�(h0j&  hF��. It is worth noting that in
many textbooks and articles where the authors uses a continous compounding then
a shift will be multiplicative.�ubeubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�0The reason for the name multiplicative_shift is:�hHh}hIK�hJ]�hZ�0The reason for the name multiplicative_shift is:���}���}�(h0jO  hFjW  ubaubj  )}���}�(hhh0h�h7}�(j  �h9]�h=]�j  X�  P(t, multiplicative\_shift) &= \exp\left(-(continous\_forward\_rate(t)
                             + multiplicative\_shift) \cdot t\right) \\
                            &= \exp\left(-continous\_forward\_rate(t) \cdot t\right)
                             \cdot \exp\left(-multiplicative\_shift \cdot t\right) \\
                            &= \left(1 + zero\_coupon\_rate\right)^{-t}
                             \cdot \left(1 + \left(\exp(multiplicative\_shift) - 1)\right)\right)^{-t} \\
                            &= \left(1 + zero\_coupon\_rate\right)^{-t}
                             \cdot \left(1 + multiplicative\_shift\_rate\right)^{-t} \\
                            &= P(t) \cdot \left(1 + multiplicative\_shift\_rate\right)^{-t}

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIK�hJ]�ubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�KHere :math:`multiplicative\_shift\_rate = \exp(multiplicative\_shift) - 1`.�hHh}hIK�hJ]�(hZ�Here ���}���}�(h0jj  hF�Here �ubj  �math���)}���}�(h0jj  h7}�(h9]�h=]��latex��=multiplicative\_shift\_rate = \exp(multiplicative\_shift) - 1�h;]�h?]�hB]�uhFhGhHjz  hJ]�ubhZ�.���}���}�(h0jj  hF�.�ubeubh�)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhFX�  So now it becomes obvious why the multiplicative shift is mathematically more
feasable than the additive. This is because the multiplicative_shift_rate is
independent of the original yieldcurve when discounting or compounding.

In other words the discounting can be done in 2 steps, first discount the
cashflows with regard to yieldcurve and discount the discount the discounted
cashflows with regard to a simple yieldcurve with a constant rate
(the multiplicative_shift_rate).�hHh�hINhJ]�(h~)}���}�(h0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��So now it becomes obvious why the multiplicative shift is mathematically more
feasable than the additive. This is because the multiplicative_shift_rate is
independent of the original yieldcurve when discounting or compounding.�hHh}hIK�hJ]�hZ��So now it becomes obvious why the multiplicative shift is mathematically more
feasable than the additive. This is because the multiplicative_shift_rate is
independent of the original yieldcurve when discounting or compounding.���}���}�(h0j�  hFj�  ubaubh~)}���}�(h0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��In other words the discounting can be done in 2 steps, first discount the
cashflows with regard to yieldcurve and discount the discount the discounted
cashflows with regard to a simple yieldcurve with a constant rate
(the multiplicative_shift_rate).�hHh}hIK�hJ]�hZ��In other words the discounting can be done in 2 steps, first discount the
cashflows with regard to yieldcurve and discount the discount the discounted
cashflows with regard to a simple yieldcurve with a constant rate
(the multiplicative_shift_rate).���}���}�(h0j�  hFj�  ubaubeubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�QAnyway the additive_shift cannot be ignored since there are quotes related to it.�hHh}hIK�hJ]�hZ�QAnyway the additive_shift cannot be ignored since there are quotes related to it.���}���}�(h0j�  hFj�  ubaubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��The relationship between the additive_shift and the multiplicative_shift_rate
can be seen from looking at the discount factors for a specific future time t
expressed though the zero_coupon_rate:�hHh}hIK�hJ]�hZ��The relationship between the additive_shift and the multiplicative_shift_rate
can be seen from looking at the discount factors for a specific future time t
expressed though the zero_coupon_rate:���}���}�(h0j�  hFj�  ubaubj  )}���}�(hhh0h�h7}�(j  �h9]�h=]�j  X  \left(1 + zero\_coupon\_rate + additive\_shift\right)^{-t} &=
     \left(1 + zero\_coupon\_rate\right)^{-t}
     \cdot \left(1 + multiplicative\_shift\_rate\right)^{-t} \\
     & \Updownarrow \\
additive\_shift &= multiplicative\_shift\_rate \cdot (1 + zero\_coupon\_rate)

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIK�hJ]�ubh~)}���}�(hhh0h�h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��Note however that when additive_shift and multiplicative_shift_rate are calculated
for more complex cashflows (ie more than 1 payment) then the relation isn't that
simple.�hHh}hIK�hJ]�hZ��Note however that when additive_shift and multiplicative_shift_rate are calculated
for more complex cashflows (ie more than 1 payment) then the relation isn't that
simple.���}���}�(h0j�  hFj�  ubaubeubeubh7}�(h9]�hah;]�h=]�h?]�� the-basic-design-for-yieldcurves�ahB]�uhDhEhFhGhHh+hIKhJ]�(hM)}���}�(hhh0h2h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF� The basic design for yieldcurves�hHhLhIKhJ]�hZ� The basic design for yieldcurves���}���}�(h0j�  hFj  ubaubh~)}���}�(hhh0h2h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�lHere is the design and the definitions of the functionality of yieldcurve base class in the finance package.�hHh}hIKhJ]�hZ�lHere is the design and the definitions of the functionality of yieldcurve base class in the finance package.���}���}�(h0j  hFj  ubaubh~)}���}�(hhh0h2h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�2First of all there are some generel functionality:�hHh}hIKhJ]�hZ�2First of all there are some generel functionality:���}���}�(h0j  hFj%  ubaubh �bullet_list���)}���}�(hhh0h2h7}�(h9]�h=]�h;]��bullet��*�h?]�hB]�uhDhEhFhGhHj,  hIK!hJ]�(h �	list_item���)}���}�(hhh0j/  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�f**add_function** It should be possible dynamically to add more yield curve
functions to a yield curve
�hHj:  hINhJ]�h~)}���}�(h0j=  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�e**add_function** It should be possible dynamically to add more yield curve
functions to a yield curve�hHh}hIK!hJ]�(j7  )}���}�(h0jH  h7}�(h9]�h;]�h=]�h?]�hB]�uhF�**add_function**�hHj6  hJ]�hZ�add_function���}���}�(h0jS  hFhGubaubhZ�U It should be possible dynamically to add more yield curve
functions to a yield curve���}���}�(h0jH  hF�U It should be possible dynamically to add more yield curve
functions to a yield curve�ubeubaubj;  )}���}�(hhh0j/  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�;**yieldcurve_functions** Return the list of used functions
�hHj:  hINhJ]�h~)}���}�(h0ji  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�:**yieldcurve_functions** Return the list of used functions�hHh}hIK$hJ]�(j7  )}���}�(h0jt  h7}�(h9]�h;]�h=]�h?]�hB]�uhF�**yieldcurve_functions**�hHj6  hJ]�hZ�yieldcurve_functions���}���}�(h0j  hFhGubaubhZ�" Return the list of used functions���}���}�(h0jt  hF�" Return the list of used functions�ubeubaubj;  )}���}�(hhh0j/  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�p**Print a yieldcurve** When printed a yield curve shows the string
representation for all yield curve functions
�hHj:  hINhJ]�h~)}���}�(h0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�o**Print a yieldcurve** When printed a yield curve shows the string
representation for all yield curve functions�hHh}hIK&hJ]�(j7  )}���}�(h0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhF�**Print a yieldcurve**�hHj6  hJ]�hZ�Print a yieldcurve���}���}�(h0j�  hFhGubaubhZ�Y When printed a yield curve shows the string
representation for all yield curve functions���}���}�(h0j�  hF�Y When printed a yield curve shows the string
representation for all yield curve functions�ubeubaubeubh~)}���}�(hhh0h2h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�aAnd then there are the pure calculation functionality which are defined in the
subsections below.�hHh}hIK)hJ]�hZ�aAnd then there are the pure calculation functionality which are defined in the
subsections below.���}���}�(h0j�  hFj�  ubaubh,)}���}�(hhh0h2h7}�(h9]�hah;]�h=]�h?]��continous-forward-rate�ahB]�uhDhEhFhGhHh+hIK-hJ]�(hM)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�continous_forward_rate�hHhLhIK-hJ]�hZ�continous_forward_rate���}���}�(h0j�  hFj�  ubaubh~)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�uThis is the base function from which everything else is derived. At time t it is
the average rate from now till then.�hHh}hIK/hJ]�hZ�uThis is the base function from which everything else is derived. At time t it is
the average rate from now till then.���}���}�(h0j�  hFj�  ubaubh �warning���)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��Since this is the base a future estimation/calibration procedure must also be
based on continous_forward_rate. I.e. this is the function that needs to be
optimized.�hHj�  hINhJ]�h~)}���}�(h0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��Since this is the base a future estimation/calibration procedure must also be
based on continous_forward_rate. I.e. this is the function that needs to be
optimized.�hHh}hIK3hJ]�hZ��Since this is the base a future estimation/calibration procedure must also be
based on continous_forward_rate. I.e. this is the function that needs to be
optimized.���}���}�(h0j	  hFj  ubaubaubeubh,)}���}�(hhh0h2h7}�(h9]�hah;]�h=]�h?]��continous-rate-timeslope�ahB]�uhDhEhFhGhHh+hIK9hJ]�(hM)}���}�(hhh0j  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�continous_rate_timeslope�hHhLhIK9hJ]�hZ�continous_rate_timeslope���}���}�(h0j$  hFj,  ubaubh~)}���}�(hhh0j  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��The continous_rate_timeslope is the first order derivative with regard to time.
It is used in others calculations. The formula is:�hHh}hIK;hJ]�hZ��The continous_rate_timeslope is the first order derivative with regard to time.
It is used in others calculations. The formula is:���}���}�(h0j4  hFj<  ubaubj  )}���}�(hhh0j  h7}�(j  �h9]�h=]�j  �Ycontinous\_rate\_timeslope(t) = \frac{\partial continous\_forward\_rate(t)}{\partial t}

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIK>hJ]�ubeubh,)}���}�(hhh0h2h7}�(h9]�hah;]�h=]�h?]��instantanious-forward-rate�ahB]�uhDhEhFhGhHh+hIKBhJ]�(hM)}���}�(hhh0jO  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�instantanious_forward_rate�hHhLhIKBhJ]�hZ�instantanious_forward_rate���}���}�(h0jZ  hFjb  ubaubh~)}���}�(hhh0jO  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��Actually in all textbooks and articles the continous_forward_rate is defined by
as the average over time of the instantanious_forward_rate.�hHh}hIKDhJ]�hZ��Actually in all textbooks and articles the continous_forward_rate is defined by
as the average over time of the instantanious_forward_rate.���}���}�(h0jj  hFjr  ubaubh~)}���}�(hhh0jO  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�LBut for computational purposes it is better to reverse it. So by definition:�hHh}hIKGhJ]�hZ�LBut for computational purposes it is better to reverse it. So by definition:���}���}�(h0jz  hFj�  ubaubj  )}���}�(hhh0jO  h7}�(j  �h9]�h=]�j  �`continous\_forward\_rate(t) = \frac{1}{t}\int_{0}^{t}instantanious\_forward\_rate(x)\partial x

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIKIhJ]�ubh~)}���}�(hhh0jO  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�And then it follows:�hHh}hIKLhJ]�hZ�And then it follows:���}���}�(h0j�  hFj�  ubaubj  )}���}�(hhh0jO  h7}�(j  �h9]�h=]�j  X8  instantanious\_forward\_rate(t) &= \frac{\partial \left(t \cdot continous\_forward\_rate(t)\right)}{\partial t} \\
&= continous\_forward\_rate(t) \\
& \quad + t \cdot \frac{\partial continous\_forward\_rate(t)}{\partial t} \\
&= continous\_forward\_rate(t) \\
& \quad + t \cdot continous\_forward\_timeslope(t)

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIKNhJ]�ubeubh,)}���}�(hhh0h2h7}�(h9]�hah;]�h=]�h?]��discount-factor�ahB]�uhDhEhFhGhHh+hIKVhJ]�(hM)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�discount_factor�hHhLhIKVhJ]�hZ�discount_factor���}���}�(h0j�  hFj�  ubaubh~)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�uThe discount_factor is giving the present value of the amount of 1 unit at a
future time t, P(t). And the formula is:�hHh}hIKXhJ]�hZ�uThe discount_factor is giving the present value of the amount of 1 unit at a
future time t, P(t). And the formula is:���}���}�(h0j�  hFj�  ubaubj  )}���}�(hhh0j�  h7}�(j  �h9]�h=]�j  ��P(t) &= \exp\left(-\int_{0}^{t}instantanious\_forward\_rate(x)\partial x\right) \\
     &= \exp\left(-continous\_forward\_rate(t) \cdot t\right)

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIK[hJ]�ubh~)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�`When a yieldcurve is called as a python function the yieldcurve will return the
discount_factor.�hHh}hIK_hJ]�hZ�`When a yieldcurve is called as a python function the yieldcurve will return the
discount_factor.���}���}�(h0j�  hFj�  ubaubeubh.h,)}���}�(hhh0h2h7}�(h9]�hah;]�h=]�h?]��discrete-forward-rate�ahB]�uhDhEhFhGhHh+hIK~hJ]�(hM)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�discrete_forward_rate�hHhLhIK~hJ]�hZ�discrete_forward_rate���}���}�(h0j  hFj	  ubaubh~)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��The discrete_forward_rate is the average rate between 2 future times :math:`t_1`
and :math:`t_2`.
The forward rate is defined as (using no arbitrage):�hHh}hIK�hJ]�(hZ�EThe discrete_forward_rate is the average rate between 2 future times ���}���}�(h0j  hF�EThe discrete_forward_rate is the average rate between 2 future times �ubj{  )}���}�(h0j  h7}�(h9]�h=]��latex��t_1�h;]�h?]�hB]�uhFhGhHjz  hJ]�ubhZ�
and ���}���}�(h0j  hF�
and �ubj{  )}���}�(h0j  h7}�(h9]�h=]��latex��t_2�h;]�h?]�hB]�uhFhGhHjz  hJ]�ubhZ�6.
The forward rate is defined as (using no arbitrage):���}���}�(h0j  hF�6.
The forward rate is defined as (using no arbitrage):�ubeubj  )}���}�(hhh0j�  h7}�(j  �h9]�h=]�j  ��(1 + zero\_coupon\_rate(t_2))^{t_2} &= (1 + zero\_coupon\_rate(t_1))^{t_1} \\
& \quad \cdot (1 + discrete\_forward\_rate(t_1, t_2))^{t_2 - t_1}

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIK�hJ]�ubh~)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�Taking the log gives:�hHh}hIK�hJ]�hZ�Taking the log gives:���}���}�(h0jQ  hFjY  ubaubj  )}���}�(hhh0j�  h7}�(j  �h9]�h=]�j  ��& continous\_forward\_rate(t_2) \cdot t_2 = \\
& \quad continous\_forward\_rate(t_1) \cdot t_1 \\
& \quad + \ln (1 + discrete\_forward\_rate(t_1, t_2)) \cdot (t_2 - t_1)

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIK�hJ]�ubh~)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�or�hHh}hIK�hJ]�hZ�or���}���}�(h0jl  hFjt  ubaubj  )}���}�(hhh0j�  h7}�(j  �h9]�h=]�j  ��& discrete\_forward\_rate(t_1, t_2) = \\
& \quad \exp \left(\frac{continous\_forward\_rate(t_2) \cdot t_2
     - continous\_forward\_rate(t_1) \cdot t_1}{t_2 - t_1}\right) - 1

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIK�hJ]�ubh~)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�jThis way the discrete_forward_rate is the descretized time weighted average of the
continous_forward_rate.�hHh}hIK�hJ]�hZ�jThis way the discrete_forward_rate is the descretized time weighted average of the
continous_forward_rate.���}���}�(h0j�  hFj�  ubaubh�)}���}�(hhh0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�YIf one of the times are zero then formula reduces to the formula for the
zero_coupon_rate�hHh�hINhJ]�h~)}���}�(h0j�  h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�YIf one of the times are zero then formula reduces to the formula for the
zero_coupon_rate�hHh}hIK�hJ]�hZ�YIf one of the times are zero then formula reduces to the formula for the
zero_coupon_rate���}���}�(h0j�  hFj�  ubaubaubeubeubh7}�(h9]�hah;]�h=]�h?]�h*ahB]�uhDhEhFhGhHh+hIKchJ]�(hM)}���}�(hhh0h.h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�zero_coupon_rate�hHhLhIKchJ]�hZ�zero_coupon_rate���}���}�(h0j�  hFj�  ubaubh~)}���}�(hhh0h.h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF��The definition of zero_coupon_rate comes from the bond and deposit market. It is
a bit like the continous_forward_rate except for how the discounting is done.
Here the discount_factor is:�hHh}hIKehJ]�hZ��The definition of zero_coupon_rate comes from the bond and deposit market. It is
a bit like the continous_forward_rate except for how the discounting is done.
Here the discount_factor is:���}���}�(h0j�  hFj�  ubaubj  )}���}�(hhh0h.h7}�(j  �h9]�h=]�j  �1P(t) = \left(1 + zero\_coupon\_rate\right)^{-t}

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIKihJ]�ubh~)}���}�(hhh0h.h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�RCombining the definition of the discount_factor and the zero_coupon_rate one
gets:�hHh}hIKlhJ]�hZ�RCombining the definition of the discount_factor and the zero_coupon_rate one
gets:���}���}�(h0j�  hFj�  ubaubj  )}���}�(hhh0h.h7}�(j  �h9]�h=]�j  �b\exp\left(-continous\_forward\_rate(t) \cdot t\right) = \left(1 + zero\_coupon\_rate\right)^{-t}

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIKohJ]�ubh~)}���}�(hhh0h.h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�or:�hHh}hIKrhJ]�hZ�or:���}���}�(h0j�  hFj  ubaubj  )}���}�(hhh0h.h7}�(j  �h9]�h=]�j  �Fcontinous\_forward\_rate(t) = \ln\left(1 + zero\_coupon\_rate\right)

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIKthJ]�ubh~)}���}�(hhh0h.h7}�(h9]�h;]�h=]�h?]�hB]�uhDhEhF�rSo there is a simple relation between the continous_forward_rate and the
zero_coupon_rate. The reverse formula is:�hHh}hIKwhJ]�hZ�rSo there is a simple relation between the continous_forward_rate and the
zero_coupon_rate. The reverse formula is:���}���}�(h0j  hFj"  ubaubj  )}���}�(hhh0h.h7}�(j  �h9]�h=]�j  �=zero\_coupon\_rate = \exp (continous\_forward\_rate(t)) - 1

�h;]�j  Nj  j   h?]�hB]�j#  NuhDhEhFhGhHj  hIKzhJ]�ubeubh�h�j�  j�  j�  j�  jV  jO  hAh5j   j  j�  h2j�  j�  h�h�u�current_line�N�id_start�K�symbol_footnotes�]��transform_messages�]��footnote_refs�}��reporter�N�settings��docutils.frontend��Values���)}���}�(�toc_backlinks��entry��strip_classes�N�syntax_highlight��long��file_insertion_enabled���trim_footnote_reference_space���source_link�N�language_code��en��auto_id_prefix��id��pep_base_url�� https://www.python.org/dev/peps/��	traceback���report_level�K�_source�hE�_disable_config�N�output_encoding��utf-8��strip_elements_with_classes�N�warning_stream�N�_destination�N�expose_internals�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�	id_prefix�hG�exit_status_level�K�dump_transforms�N�raw_enabled�K�docinfo_xform�K�error_encoding_error_handler��backslashreplace��error_encoding��UTF-8�hLN�pep_file_url_template��pep-%04d��
halt_level�K�dump_pseudo_xml�N�sectsubtitle_xform���	generator�N�debug�N�dump_internals�N�	datestamp�N�strip_comments�N�strict_visitor�N�output_encoding_error_handler��strict��sectnum_xform�K�_config_files�]��dump_settings�N�rfc_references�N�doctitle_xform���cloak_email_addresses���embed_stylesheet���input_encoding��	utf-8-sig��footnote_backlinks�K�pep_references�N�env�N�input_encoding_error_handler�jt  �gettext_compact���record_dependencies�N�config�N�
source_url�N�smart_quotes��ub�parse_messages�]��	citations�]��nameids�}�(hhAhh�hh*hjV  hj�  hj�  hj�  hj�  hj   hh�u�transformer�Nhhh7}�(h9]�h=]�h;]��source�hEh?]�hB]�u�citation_refs�}�hFhGhHhhJ]�h5aub.